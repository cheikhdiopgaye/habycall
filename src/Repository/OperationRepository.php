<?php

namespace App\Repository;

use App\Entity\Operation;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Operation|null find($id, $lockMode = null, $lockVersion = null)
 * @method Operation|null findOneBy(array $criteria, array $orderBy = null)
 * @method Operation[]    findAll()
 * @method Operation[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class OperationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Operation::class);
    }

    /**
    * @param $debut
    * @param $fin
    * @param $user
    * @return Operations[] Returns an array of Operations objects
    */
    public function findByPeriode($debut,$fin,$user): array
    {
        $qb = $this->createQueryBuilder('p')
            ->andWhere('p.dateoperation >= :debut' )
            ->andWhere('p.dateoperation <= :fin ' )
            ->addSelect('r')
            ->leftJoin('p.user','r')
            ->andWhere('r.user =:val')
            ->setParameter('debut', $debut->format('Y-m-d') . ' 00:00:00')
            ->setParameter('fin', $fin->format('Y-m-d') . ' 23:59:59')
            ->setParameter('val', $user)

            ->getQuery();
        return $qb->execute();
    }

    // /**
    //  * @return Operation[] Returns an array of Operation objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('o.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Operation
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
