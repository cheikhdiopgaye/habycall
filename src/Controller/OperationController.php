<?php

namespace App\Controller;

use App\Entity\User;
use App\Entity\Client;
use App\Entity\Compte;
use App\Entity\Operation;
use App\Form\OperationType;
use App\Repository\OperationRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Constraints\DateTime;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 *  @Route("/api")
 */
class OperationController extends AbstractController
{
    /**
     * @Route("/security/operation", name="operation", methods={"POST"})
     */
    public function index(Request $request, EntityManagerInterface $entityManager, SerializerInterface $serializer,ValidatorInterface $validator)
    {        
                $depot = new Operation();
                $connecte=$this->getUser();
                $user= $this->getDoctrine()->getRepository(User::class)->find($connecte);
                $user->addResponsable($depot);
                $jour = date('d');
                $aa= date('gi');
                $mm=date('m');
                $ss=date('s');
                $numero=$aa.$jour.$mm.$ss;
                $depot->setNumoperation($numero);
                $depot->setDateoperation(new \DateTime());
                $depot->setTypeofoperation('Rechargement');
                $form = $this->createForm(OperationType::class, $depot);
                $Values=$request->request->all();
                $form->submit($Values);
                $montant=$Values['montant'];
                $depot->setMontant($montant);
                $numerocompte=$Values['numerocompte'];
                $compte = $this->getDoctrine()->getRepository(Compte::class)->
                    findOneBy(["numerocompte"=>$numerocompte]);
                $compte->setSolde($compte->getSolde()+ $montant);

                $compte->addOperation($depot);
                if($montant<"1000") {
                    $data = [
                        'status-0' => 500,
                        'message-0' =>"Le montant de dépot minimum autorisée est de 1000 fr",
                    ];
            
                    return new JsonResponse($data, 500);
                } else{
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($depot);
            $entityManager->persist($compte);
            $entityManager->flush();
        return new Response('Le compte a été bien crédité',Response::HTTP_CREATED); 
        } 
    }

    /**
     * @Route("/security/debiter", name="debiter", methods={"POST"})
     */
    public function debiter (Request $request, EntityManagerInterface $entityManager, SerializerInterface $serializer,ValidatorInterface $validator)
    {        
                $depot = new Operation();
                $connecte=$this->getUser();
                $user= $this->getDoctrine()->getRepository(User::class)->find($connecte);
                $user->addResponsable($depot);
                $jour = date('d');
                $aa= date('gi');
                $mm=date('m');
                $ss=date('s');
                $numero=$aa.$jour.$mm.$ss;
                $depot->setNumoperation($numero);
                $depot->setDateoperation(new \DateTime());
                $depot->setTypeofoperation('Paiement');
                $form = $this->createForm(OperationType::class, $depot);
                $Values=$request->request->all();
                $form->submit($Values);
                $montant=$Values['montant'];
                $depot->setMontant($montant);
                $numerocompte=$Values['numerocompte'];
                $compte = $this->getDoctrine()->getRepository(Compte::class)->
                    findOneBy(["numerocompte"=>$numerocompte]);
                $compte->setSolde($compte->getSolde() - $montant);

                $compte->addOperation($depot);
                if($compte->getSolde()<$montant || $compte->getSolde()<=0) {
                    $data = [
                        'status-0' => 403,
                        'message-0' =>"Le solde de votre compte ne vous permet pas de debiter ce compte !",
                    ];
            
                    return new JsonResponse($data, 500);
                } else{
                    $entityManager = $this->getDoctrine()->getManager();
                    $entityManager->persist($depot);
                    $entityManager->persist($compte);
                    $entityManager->flush();
                    return new Response('Le compte a été bien débité',Response::HTTP_CREATED); 
        } 
    }

/**
 * @Route("/security/lister/operation", name="user_operation", methods={"GET"})
 *
 */
public function ListerOperation(OperationRepository $operationRepository, SerializerInterface $serializer)
{   $connecte = $this->getUser();
    $operation = $operationRepository->findAll();
    $users = $serializer->serialize($operation, 'json', ['groups' => ['show']]);
    return new Response($users, 200, [
        'Content-Type' => 'application/json'
    ]);
}

    /**
     * @Route("/listerperiode", name="periode_liste_id", methods={"POST","GET"})
     */

    public function detailsOperation(ValidatorInterface $validator, Request $request, OperationRepository $operationsRepository,  SerializerInterface $serializer)
    {
        $data = json_decode($request->getContent(), true);

        $debut = $data['debut'];
        $fin = $data['fin'];

        $user = $this->getUser()->getId();
    
        if (empty($debut) && empty($fin)) {

            $debut = (new DateTime);
            $fin = (new DateTime);
        
        } elseif (empty($debut) && !empty($fin)) {

            $debut = (new \DateTime($fin));
            $fin = (new \DateTime($fin));
        } elseif (!empty($debut) && empty($fin)) {

            $debut = (new \DateTime($debut));
            $fin = (new \DateTime());

        } elseif ((!empty($debut)) && (!empty($fin))) {
            $debut = (new \DateTime($debut));
            $fin = (new \DateTime($fin));
        }
        $operations = $operationsRepository->findByPeriode($debut, $fin, $user);
        
        $tab = [];
        $tab[0] = $operations;
        $dat = $serializer->serialize($tab, 'json', ['groups' => ['show']]);

        return new Response($dat, 200, [
            'Content-Type' => 'application/json'
        ]);
    }

    /**
    * @Route("/security/lister/crediter", name="nombre_crediter", methods={"GET"})
    * 
    */
     public function crediterliste( OperationRepository  $operationRepository, SerializerInterface $serializer)
    {
       /*  $typ= $typeRepository->findOneBy(['id'=> 1]); */

        $type= $operationRepository->findBy(['typeofoperation'=> 'Rechargement']);
        //dd(count($type));
        $nombre = count($type);
        $data=[$nombre];
        $users = $serializer->serialize($data, 'json', ['groups' => ['show']]);
       return new Response($users, 200, [
        'Content-Type' => 'application/json'
    ]);
    }

    /**
    * @Route("/security/lister/paiements", name="nombrePaiement", methods={"GET"})
    * 
    */
    public function NombrePaiement( OperationRepository  $operationRepository, SerializerInterface $serializer)
    {
        $type= $operationRepository->findBy(['typeofoperation'=> 'Paiement']);
        $nombre = count($type);
        $data=[$nombre];
        $users = $serializer->serialize($data, 'json', ['groups' => ['show']]);
       return new Response($users, 200, [
        'Content-Type' => 'application/json'
    ]);
    }

    /**
    * @Route("/security/lister/nombreoperation", name="nombreoperation", methods={"GET"})
    * 
    */
    public function NombreOperation( OperationRepository  $operationRepository, SerializerInterface $serializer)
    {
        $type= $operationRepository->findAll();
        $nombre = count($type);
        $data=[$nombre];
        $users = $serializer->serialize($data, 'json', ['groups' => ['show']]);
       return new Response($users, 200, [
        'Content-Type' => 'application/json'
    ]);
    }

    /**
    * @Route("/security/lister/Rechargement", name="nombre_rechargement", methods={"GET"})
    * @IsGranted({"ROLE_RESPONSABLE"}, statusCode=403, message="Vous n'avez pas accès à cette page !")
    */
    public function RechargementList( OperationRepository  $operationRepository, SerializerInterface $serializer)
    {
        $Rechargement='Rechargement';
        $connecte = $this->getUser();
        $data= $operationRepository->findBy(['typeofoperation'=>$Rechargement, 'user'=>$connecte]);
        $Rechargement = $serializer->serialize($data, 'json', ['groups' => ['show']]);
       return new Response($Rechargement, 200, [
        'Content-Type' => 'application/json'
    ]);
    }
    /**
    * @Route("/security/lister/Paiement", name="nombre_paiement", methods={"GET"})
    * @IsGranted({"ROLE_RESPONSABLE"}, statusCode=403, message="Vous n'avez pas accès à cette page !")
    */
    public function PaiementList( OperationRepository  $operationRepository, SerializerInterface $serializer)
    {
        $Rechargement='Paiement';
        $connecte = $this->getUser();
        $data= $operationRepository->findBy(['typeofoperation'=>$Rechargement, 'user'=>$connecte]);
        $Rechargement = $serializer->serialize($data, 'json', ['groups' => ['show']]);
       return new Response($Rechargement, 200, [
        'Content-Type' => 'application/json'
    ]);
    }

     /**
    * @Route("/security/lister/debiter", name="nombre_debiter", methods={"GET"})
    * 
    */
    public function debiterliste( OperationRepository  $operationRepository, SerializerInterface $serializer)
    {
      /*   $typ= $typeRepository->findOneBy(['id'=> 2]); */
        $type= $operationRepository->findBy(['typeoperation'=> 'Paiement']);
        //dd(count($type));
        $nombre = count($type);
        $data=[$nombre];
        $users = $serializer->serialize($data, 'json', ['groups' => ['show']]);
       return new Response($users, 200, [
        'Content-Type' => 'application/json'
    ]);
    }
}
