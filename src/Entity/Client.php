<?php

namespace App\Entity;

use App\Entity\Compte;
use Symfony\Component\Validator\Constraints as Assert; //pour la validation des données
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Serializer\Annotation\Groups;


/**
 * @ORM\Entity(repositoryClass="App\Repository\ClientRepository")
 */
class Client
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     *  @Groups({"show"})
     */
    private $id;

     /**
     * @ORM\Column(type="string", nullable=true)
     *  @Groups({"show"})
    */
    private $adresse;


    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     *  @Groups({"show"})
     */
    private $civilite;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     *  @Groups({"show"})
     */
    private $commentaire;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     *  @Groups({"show"})
     */
    private $matricule;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"show"})
     */
    private $nomcomplet;

   /**
     * @ORM\Column(type="integer", nullable=true)
     * @Assert\NotBlank(message="Le téléphone du responsable ne doit pas être vide")
     *  @Groups({"show"})
     * @Assert\Regex(
     *     pattern="/^(\+[1-9][0-9]*(\([0-9]*\)|-[0-9]*-))?[0]?[1-9][0-9\-]*$/",
     *     match=true,
     *     message="Votre numero ne doit pas contenir de lettre"
     * )
    */
    private $mobile;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Compte", mappedBy="clients")
     * @Groups({"show"})
     */
    private $client;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="clients")
     * @Groups({"show"})
     */
    private $user;


    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $datenaisse;

    public function __construct()
    {
        $this->client = new ArrayCollection();
    }




    public function getId(): ?int
    {
        return $this->id;
    }


    public function getAdresse(): ?String
    {
        return $this->adresse;
    }

    public function setAdresse(?String $adresse): self
    {
        $this->adresse = $adresse;

        return $this;
    }

    public function getCivilite(): ?string
    {
        return $this->civilite;
    }

    public function setCivilite(?string $civilite): self
    {
        $this->civilite = $civilite;

        return $this;
    }

    public function getCommentaire(): ?string
    {
        return $this->commentaire;
    }

    public function setCommentaire(?string $commentaire): self
    {
        $this->commentaire = $commentaire;

        return $this;
    }

    public function getMatricule(): ?string
    {
        return $this->matricule;
    }

    public function setMatricule(?string $matricule): self
    {
        $this->matricule = $matricule;

        return $this;
    }

  

    public function getNomcomplet(): ?string
    {
        return $this->nomcomplet;
    }

    public function setNomcomplet(string $nomcomplet): self
    {
        $this->nomcomplet = $nomcomplet;

        return $this;
    }

    public function getMobile(): ?string
    {
        return $this->mobile;
    }

    public function setMobile(string $mobile): self
    {
        $this->mobile = $mobile;

        return $this;
    }

    /**
     * @return Collection|Compte[]
     */
    public function getClient(): Collection
    {
        return $this->client;
    }

    public function addClient(Compte $client): self
    {
        if (!$this->client->contains($client)) {
            $this->client[] = $client;
            $client->setClients($this);
        }

        return $this;
    }

    public function removeClient(Compte $client): self
    {
        if ($this->client->contains($client)) {
            $this->client->removeElement($client);
            // set the owning side to null (unless already changed)
            if ($client->getClients() === $this) {
                $client->setClients(null);
            }
        }

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

  

    public function getDatenaisse(): ?\DateTimeInterface
    {
        return $this->datenaisse;
    }

    public function setDatenaisse(?\DateTimeInterface $datenaisse): self
    {
        $this->datenaisse = $datenaisse;

        return $this;
    }





}
