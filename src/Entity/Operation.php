<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass="App\Repository\OperationRepository")
 */
class Operation
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     *  @Groups({"show"})
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     *  @Groups({"show"})
     */
    private $numoperation;

    /**
     * @ORM\Column(type="integer")
     *  @Groups({"show"})
     */
    private $montant;

    /**
     * @ORM\Column(type="date")
     *  @Groups({"show"})
     */
    private $dateoperation;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="responsable")
     * @Groups({"show"})
     */
    private $user;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Compte", inversedBy="operation")
     * @Groups({"show"})
     */
    private $compte;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"show"})
     */
    private $typeofoperation;


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNumoperation(): ?int
    {
        return $this->numoperation;
    }

    public function setNumoperation(int $numoperation): self
    {
        $this->numoperation = $numoperation;

        return $this;
    }

    public function getMontant(): ?int
    {
        return $this->montant;
    }

    public function setMontant(int $montant): self
    {
        $this->montant = $montant;

        return $this;
    }

    public function getDateoperation(): ?\DateTimeInterface
    {
        return $this->dateoperation;
    }

    public function setDateoperation(\DateTimeInterface $dateoperation): self
    {
        $this->dateoperation = $dateoperation;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getCompte(): ?Compte
    {
        return $this->compte;
    }

    public function setCompte(?Compte $compte): self
    {
        $this->compte = $compte;

        return $this;
    }


    public function getTypeofoperation(): ?string
    {
        return $this->typeofoperation;
    }

    public function setTypeofoperation(string $typeofoperation): self
    {
        $this->typeofoperation = $typeofoperation;

        return $this;
    }

    
}
