<?php

/**
 * This file has been auto-generated
 * by the Symfony Routing Component.
 */

return [
    false, // $matchHost
    [ // $staticRoutes
        '/_profiler' => [[['_route' => '_profiler_home', '_controller' => 'web_profiler.controller.profiler::homeAction'], null, null, null, true, false, null]],
        '/_profiler/search' => [[['_route' => '_profiler_search', '_controller' => 'web_profiler.controller.profiler::searchAction'], null, null, null, false, false, null]],
        '/_profiler/search_bar' => [[['_route' => '_profiler_search_bar', '_controller' => 'web_profiler.controller.profiler::searchBarAction'], null, null, null, false, false, null]],
        '/_profiler/phpinfo' => [[['_route' => '_profiler_phpinfo', '_controller' => 'web_profiler.controller.profiler::phpinfoAction'], null, null, null, false, false, null]],
        '/_profiler/open' => [[['_route' => '_profiler_open_file', '_controller' => 'web_profiler.controller.profiler::openAction'], null, null, null, false, false, null]],
        '/api/security/operation' => [[['_route' => 'operation', '_controller' => 'App\\Controller\\OperationController::index'], null, ['POST' => 0], null, false, false, null]],
        '/api/security/debiter' => [[['_route' => 'debiter', '_controller' => 'App\\Controller\\OperationController::debiter'], null, ['POST' => 0], null, false, false, null]],
        '/api/security/lister/operation' => [[['_route' => 'user_operation', '_controller' => 'App\\Controller\\OperationController::ListerOperation'], null, ['GET' => 0], null, false, false, null]],
        '/api/listerperiode' => [[['_route' => 'periode_liste_id', '_controller' => 'App\\Controller\\OperationController::detailsOperation'], null, ['POST' => 0, 'GET' => 1], null, false, false, null]],
        '/api/security/lister/crediter' => [[['_route' => 'nombre_crediter', '_controller' => 'App\\Controller\\OperationController::crediterliste'], null, ['GET' => 0], null, false, false, null]],
        '/api/security/lister/paiements' => [[['_route' => 'nombrePaiement', '_controller' => 'App\\Controller\\OperationController::NombrePaiement'], null, ['GET' => 0], null, false, false, null]],
        '/api/security/lister/nombreoperation' => [[['_route' => 'nombreoperation', '_controller' => 'App\\Controller\\OperationController::NombreOperation'], null, ['GET' => 0], null, false, false, null]],
        '/api/security/lister/Rechargement' => [[['_route' => 'nombre_rechargement', '_controller' => 'App\\Controller\\OperationController::RechargementList'], null, ['GET' => 0], null, false, false, null]],
        '/api/security/lister/Paiement' => [[['_route' => 'nombre_paiement', '_controller' => 'App\\Controller\\OperationController::PaiementList'], null, ['GET' => 0], null, false, false, null]],
        '/api/security/lister/debiter' => [[['_route' => 'nombre_debiter', '_controller' => 'App\\Controller\\OperationController::debiterliste'], null, ['GET' => 0], null, false, false, null]],
        '/api/logincheck' => [[['_route' => 'login', '_controller' => 'App\\Controller\\SecurityController::login'], null, ['POST' => 0, 'GET' => 1], null, false, false, null]],
        '/api/resetting' => [[['_route' => 'resetting', '_controller' => 'App\\Controller\\SecurityController::resetPassword'], null, ['GET' => 0, 'POST' => 1], null, false, false, null]],
        '/api/inscription' => [[['_route' => 'inscription', '_controller' => 'App\\Controller\\UserController::inscriptAdmin'], null, ['POST' => 0], null, false, false, null]],
        '/api/security/client' => [[['_route' => 'client', '_controller' => 'App\\Controller\\UserController::creerClient'], null, ['POST' => 0], null, false, false, null]],
        '/api/security/lister/users' => [[['_route' => 'user', '_controller' => 'App\\Controller\\UserController::showUser'], null, ['GET' => 0], null, false, false, null]],
        '/api/security/lister/admins' => [[['_route' => 'admin_user', '_controller' => 'App\\Controller\\UserController::show'], null, ['GET' => 0], null, false, false, null]],
        '/api/security/lister/respo' => [[['_route' => 'respo_user', '_controller' => 'App\\Controller\\UserController::showrespo'], null, ['GET' => 0], null, false, false, null]],
        '/api/security/lister/mana' => [[['_route' => 'mana_user', '_controller' => 'App\\Controller\\UserController::showmana'], null, ['GET' => 0], null, false, false, null]],
        '/api/security/lister/nombreclient' => [[['_route' => 'nombre_client', '_controller' => 'App\\Controller\\UserController::ListerClient'], null, ['GET' => 0], null, false, false, null]],
        '/api/security/lister/compte' => [[['_route' => 'user_compte', '_controller' => 'App\\Controller\\UserController::ListerCompte'], null, ['GET' => 0], null, false, false, null]],
        '/api/security/lister/userconnecte' => [[['_route' => 'user_client', '_controller' => 'App\\Controller\\UserController::Userconnecte'], null, ['GET' => 0], null, false, false, null]],
    ],
    [ // $regexpList
        0 => '{^(?'
                .'|/_(?'
                    .'|error/(\\d+)(?:\\.([^/]++))?(*:38)'
                    .'|wdt/([^/]++)(*:57)'
                    .'|profiler/([^/]++)(?'
                        .'|/(?'
                            .'|search/results(*:102)'
                            .'|router(*:116)'
                            .'|exception(?'
                                .'|(*:136)'
                                .'|\\.css(*:149)'
                            .')'
                        .')'
                        .'|(*:159)'
                    .')'
                .')'
                .'|/api(?'
                    .'|/(?'
                        .'|admin/update/([^/]++)(*:201)'
                        .'|responsable/update/([^/]++)(*:236)'
                        .'|manager/update/([^/]++)(*:267)'
                        .'|security/(?'
                            .'|admin/bloquer/([^/]++)(*:309)'
                            .'|responsable/bloquer/([^/]++)(*:345)'
                            .'|manager/bloquer/([^/]++)(*:377)'
                        .')'
                    .')'
                    .'|(?:/(index)(?:\\.([^/]++))?)?(*:415)'
                    .'|/(?'
                        .'|docs(?:\\.([^/]++))?(*:446)'
                        .'|contexts/(.+)(?:\\.([^/]++))?(*:482)'
                    .')'
                .')'
            .')/?$}sDu',
    ],
    [ // $dynamicRoutes
        38 => [[['_route' => '_twig_error_test', '_controller' => 'twig.controller.preview_error::previewErrorPageAction', '_format' => 'html'], ['code', '_format'], null, null, false, true, null]],
        57 => [[['_route' => '_wdt', '_controller' => 'web_profiler.controller.profiler::toolbarAction'], ['token'], null, null, false, true, null]],
        102 => [[['_route' => '_profiler_search_results', '_controller' => 'web_profiler.controller.profiler::searchResultsAction'], ['token'], null, null, false, false, null]],
        116 => [[['_route' => '_profiler_router', '_controller' => 'web_profiler.controller.router::panelAction'], ['token'], null, null, false, false, null]],
        136 => [[['_route' => '_profiler_exception', '_controller' => 'web_profiler.controller.exception::showAction'], ['token'], null, null, false, false, null]],
        149 => [[['_route' => '_profiler_exception_css', '_controller' => 'web_profiler.controller.exception::cssAction'], ['token'], null, null, false, false, null]],
        159 => [[['_route' => '_profiler', '_controller' => 'web_profiler.controller.profiler::panelAction'], ['token'], null, null, false, true, null]],
        201 => [[['_route' => 'update_admin', '_controller' => 'App\\Controller\\UserController::updateAdmin'], ['id'], ['POST' => 0], null, false, true, null]],
        236 => [[['_route' => 'update_responsable', '_controller' => 'App\\Controller\\UserController::updateResponsable'], ['id'], ['POST' => 0], null, false, true, null]],
        267 => [[['_route' => 'update_manager', '_controller' => 'App\\Controller\\UserController::updateManager'], ['id'], ['POST' => 0], null, false, true, null]],
        309 => [[['_route' => 'bloquer_debloquer_admin', '_controller' => 'App\\Controller\\UserController::bloquerAdmin'], ['id'], ['GET' => 0], null, false, true, null]],
        345 => [[['_route' => 'bloquer_debloquer_responsable', '_controller' => 'App\\Controller\\UserController::bloquerResponsable'], ['id'], ['GET' => 0], null, false, true, null]],
        377 => [[['_route' => 'bloquer_debloquer_manager', '_controller' => 'App\\Controller\\UserController::bloquerManager'], ['id'], ['GET' => 0], null, false, true, null]],
        415 => [[['_route' => 'api_entrypoint', '_controller' => 'api_platform.action.entrypoint', '_format' => '', '_api_respond' => 'true', 'index' => 'index'], ['index', '_format'], null, null, false, true, null]],
        446 => [[['_route' => 'api_doc', '_controller' => 'api_platform.action.documentation', '_format' => '', '_api_respond' => 'true'], ['_format'], null, null, false, true, null]],
        482 => [
            [['_route' => 'api_jsonld_context', '_controller' => 'api_platform.jsonld.action.context', '_format' => 'jsonld', '_api_respond' => 'true'], ['shortName', '_format'], null, null, false, true, null],
            [null, null, null, null, false, false, 0],
        ],
    ],
    null, // $checkCondition
];
