<?php

namespace App\Form;

use App\Entity\Infocandidat;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class InfocandidatType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('situation')
            ->add('competence')
            ->add('resumer')
            ->add('twitter')
            ->add('linkedIn')
            ->add('datenaissance')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Infocandidat::class,
        ]);
    }
}
