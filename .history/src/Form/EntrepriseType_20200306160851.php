<?php

namespace App\Form;

use App\Entity\Entreprise;
use Symfony\Component\Console\Descriptor\TextDescriptor;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class EntrepriseType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nomentrep', TextType::class)
            ->add('logo')
            ->add('adressentrep', TextareaType::class)
            ->add('descriptionentrep', TextareaType::class)
            ->add('siteinternet', TextareaType::class)
            ->add('secteuractivite', TextareaType::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Entreprise::class,
        ]);
    }
}
