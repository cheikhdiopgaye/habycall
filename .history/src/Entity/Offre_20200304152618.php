<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert; //pour la validation des données

/**
 * @ORM\Entity(repositoryClass="App\Repository\OffreRepository")
 */
class Offre
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="date")
     * @Groups({"offre"})
     */
    private $datedebut;

    /**
     * @ORM\Column(type="date")
     * @Groups({"offre"})
     */
    private $datefin;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"offre"})
     * @Assert\NotBlank(message="Le type de contrat ne doit pas être vide")
     * @Assert\Length(min="2", max="255" ,minMessage="Le type de contrat est trop court !!")
     */
    private $typecontrat;
    /**
    * @ORM\Column(type="string", length=255)
    * @Groups({"offre"})
    * @Assert\NotBlank(message="La region ne doit pas être vide")
    * @Assert\Length(min="2", max="255" ,minMessage="La region est trop courte !!")
    */
    private $region;

    /**
    * @ORM\Column(type="string", length=255)
    * @Groups({"offre"})
    * @Assert\NotBlank(message="La description ne doit pas être vide")
    * @Assert\Length(min="2", max="255" ,minMessage="La description est trop courte !!")
    */
    private $description;
    /**
    * @ORM\Column(type="string", length=255)
    * @Groups({"offre"})
    * @Assert\NotBlank(message="Le secteur ne doit pas être vide")
    * @Assert\Length(min="2", max="255" ,minMessage="Le secteur est trop court !!")
    */
    private $secteur;

    /**
    * @ORM\Column(type="string", length=255)
    * @Groups({"offre"})
    * @Assert\NotBlank(message="Le metier ne doit pas être vide")
    * @Assert\Length(min="2", max="255" ,minMessage="Le metier est trop court !!")
    */
    private $metier;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="offre")
     * @Groups({"offre"})
     */
    private $user;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="postule")
     * @Groups({"offre"})
     */
    private $postule;


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDatedebut(): ?\DateTimeInterface
    {
        return $this->datedebut;
    }

    public function setDatedebut(\DateTimeInterface $datedebut): self
    {
        $this->datedebut = $datedebut;

        return $this;
    }

    public function getDatefin(): ?\DateTimeInterface
    {
        return $this->datefin;
    }

    public function setDatefin(\DateTimeInterface $datefin): self
    {
        $this->datefin = $datefin;

        return $this;
    }

    public function getTypecontrat(): ?string
    {
        return $this->typecontrat;
    }

    public function setTypecontrat(string $typecontrat): self
    {
        $this->typecontrat = $typecontrat;

        return $this;
    }

    public function getRegion(): ?string
    {
        return $this->region;
    }

    public function setRegion(string $region): self
    {
        $this->region = $region;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getSecteur(): ?string
    {
        return $this->secteur;
    }

    public function setSecteur(string $secteur): self
    {
        $this->secteur = $secteur;

        return $this;
    }

    public function getMetier(): ?string
    {
        return $this->metier;
    }

    public function setMetier(string $metier): self
    {
        $this->metier = $metier;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getPostule(): ?User
    {
        return $this->postule;
    }

    public function setPostule(?User $postule): self
    {
        $this->postule = $postule;

        return $this;
    }

 



}
