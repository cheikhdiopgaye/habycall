<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert; //pour la validation des données

/**
 * @ORM\Entity(repositoryClass="App\Repository\OffreRepository")
 */
class Offre
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="date")
     * @Groups({"offre"})
     */
    private $datedebut;

    /**
     * @ORM\Column(type="date")
     * @Groups({"offre"})
     */
    private $datefin;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"offre"})
     * @Assert\NotBlank(message="Le type de contrat ne doit pas être vide")
     * @Assert\Length(min="2", max="255" ,minMessage="Le type de contrat est trop court !!")
     */
    private $typecontrat;


    /**
    * @ORM\Column(type="string", length=255)
    * @Groups({"offre"})
    * @Assert\NotBlank(message="La description ne doit pas être vide")
    * @Assert\Length(min="2", max="255" ,minMessage="La description est trop courte !!")
    */
    private $description;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Rubrique", inversedBy="offres")
     */
    private $offre;

  
 


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDatedebut(): ?\DateTimeInterface
    {
        return $this->datedebut;
    }

    public function setDatedebut(\DateTimeInterface $datedebut): self
    {
        $this->datedebut = $datedebut;

        return $this;
    }

    public function getDatefin(): ?\DateTimeInterface
    {
        return $this->datefin;
    }

    public function setDatefin(\DateTimeInterface $datefin): self
    {
        $this->datefin = $datefin;

        return $this;
    }

    public function getTypecontrat(): ?string
    {
        return $this->typecontrat;
    }

    public function setTypecontrat(string $typecontrat): self
    {
        $this->typecontrat = $typecontrat;

        return $this;
    }


    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getOffre(): ?Rubrique
    {
        return $this->offre;
    }

    public function setOffre(?Rubrique $offre): self
    {
        $this->offre = $offre;

        return $this;
    }

    /**
     * @return Collection|Metier[]
     */
    public function getMetiers(): Collection
    {
        return $this->metiers;
    }

    public function addMetier(Metier $metier): self
    {
        if (!$this->metiers->contains($metier)) {
            $this->metiers[] = $metier;
            $metier->addOffr($this);
        }

        return $this;
    }

    public function removeMetier(Metier $metier): self
    {
        if ($this->metiers->contains($metier)) {
            $this->metiers->removeElement($metier);
            $metier->removeOffr($this);
        }

        return $this;
    }


 



}
