<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert; //pour la validation des données

/**
 * @ORM\Entity(repositoryClass="App\Repository\OffreRepository")
 */
class Offre
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"offre", "offreuser"})
     */
    private $id;

    /**
     * @ORM\Column(type="date")
     * @Groups({"offre", "offreuser"})
     */
    private $datedebut;

    /**
     * @ORM\Column(type="date")
     * @Groups({"offre", "offreuser"})
     */
    private $datefin;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"offre", offreuser"})
     * @Assert\NotBlank(message="Le type de contrat ne doit pas être vide")
     * @Assert\Length(min="2", max="255" ,minMessage="Le type de contrat est trop court !!")
     */
    private $typecontrat;


    /**
    * @ORM\Column(type="string", length=255)
    * @Groups({"offre", "offreuser"})
    * @Assert\NotBlank(message="La description ne doit pas être vide")
    * @Assert\Length(min="2", max="255" ,minMessage="La description est trop courte !!")
    */
    private $description;



    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Rubrique", inversedBy="offres")
     *  @Groups({"offre", "offreuser"})
     */
    private $offrerubrique;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Metier", inversedBy="offres")
     * @Groups({"offre", "offreuser"})
})
     */
    private $offremetier;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Useroffre", mappedBy="offre")
     * @Groups({"offreuser"})
     */
    private $useroffres;

    public function __construct()
    {
        $this->useroffres = new ArrayCollection();
    }

  



    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDatedebut(): ?\DateTimeInterface
    {
        return $this->datedebut;
    }

    public function setDatedebut(\DateTimeInterface $datedebut): self
    {
        $this->datedebut = $datedebut;

        return $this;
    }

    public function getDatefin(): ?\DateTimeInterface
    {
        return $this->datefin;
    }

    public function setDatefin(\DateTimeInterface $datefin): self
    {
        $this->datefin = $datefin;

        return $this;
    }

    public function getTypecontrat(): ?string
    {
        return $this->typecontrat;
    }

    public function setTypecontrat(string $typecontrat): self
    {
        $this->typecontrat = $typecontrat;

        return $this;
    }


    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }


    public function getOffrerubrique(): ?Rubrique
    {
        return $this->offrerubrique;
    }

    public function setOffrerubrique(?Rubrique $offrerubrique): self
    {
        $this->offrerubrique = $offrerubrique;

        return $this;
    }

    public function getOffremetier(): ?Metier
    {
        return $this->offremetier;
    }

    public function setOffremetier(?Metier $offremetier): self
    {
        $this->offremetier = $offremetier;

        return $this;
    }

    /**
     * @return Collection|Useroffre[]
     */
    public function getUseroffres(): Collection
    {
        return $this->useroffres;
    }

    public function addUseroffre(Useroffre $useroffre): self
    {
        if (!$this->useroffres->contains($useroffre)) {
            $this->useroffres[] = $useroffre;
            $useroffre->setOffre($this);
        }

        return $this;
    }

    public function removeUseroffre(Useroffre $useroffre): self
    {
        if ($this->useroffres->contains($useroffre)) {
            $this->useroffres->removeElement($useroffre);
            // set the owning side to null (unless already changed)
            if ($useroffre->getOffre() === $this) {
                $useroffre->setOffre(null);
            }
        }

        return $this;
    }








}
