<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CvRepository")
 */
class Cv
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"offreuser"})
     */
    private $id;

    /**
     * @ORM\Column(type="date")
     * @Groups({"offreuser"})
     */
    private $datecreation;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"offreuser"})
     */
    private $statut;


    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"offreuser"})
     */
    private $titre;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Formation", inversedBy="cvs")
     * @Groups({"offreuser"})
     */
    private $formation;


    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Langue", inversedBy="cvs")
     * @Groups({"offreuser"})
     */
    private $parle;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Experience", inversedBy="cvs")
     * @Groups({"offreuser"})
     */
    private $experience;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Infocandidat", inversedBy="cvs")
     * @Groups({"offreuser"})

     */
    private $infocandidat;


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDatecreation(): ?\DateTimeInterface
    {
        return $this->datecreation;
    }

    public function setDatecreation(\DateTimeInterface $datecreation): self
    {
        $this->datecreation = $datecreation;

        return $this;
    }

    public function getStatut(): ?string
    {
        return $this->statut;
    }

    public function setStatut(string $statut): self
    {
        $this->statut = $statut;

        return $this;
    }


    public function getTitre(): ?string
    {
        return $this->titre;
    }

    public function setTitre(string $titre): self
    {
        $this->titre = $titre;

        return $this;
    }

    public function getFormation(): ?Formation
    {
        return $this->formation;
    }

    public function setFormation(?Formation $formation): self
    {
        $this->formation = $formation;

        return $this;
    }

    public function getParle(): ?Langue
    {
        return $this->parle;
    }

    public function setParle(?Langue $parle): self
    {
        $this->parle = $parle;

        return $this;
    }

    public function getExperience(): ?Experience
    {
        return $this->experience;
    }

    public function setExperience(?Experience $experience): self
    {
        $this->experience = $experience;

        return $this;
    }

    public function getInfocandidat(): ?Infocandidat
    {
        return $this->infocandidat;
    }

    public function setInfocandidat(?Infocandidat $infocandidat): self
    {
        $this->infocandidat = $infocandidat;

        return $this;
    }
  
}
