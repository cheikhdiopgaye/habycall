<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ExperienceRepository")
 */
class Experience
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"offreuser"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"offreuser"})
     */
    private $nomentreprise;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"offreuser"})
     */
    private $poste;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"offreuser"})
     */
    private $lieu;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"offreuser"})
     */
    private $description;


    /**
     * @ORM\Column(type="date")
     * @Groups({"offreuser"})
     */
    private $datedebut;

    /**
     * @ORM\Column(type="date")
     * @Groups({"offreuser"})
     */
    private $datefin;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Cvexperience", mappedBy="experience")
     * @Groups({"offreuser"})
     */
    private $cvexperiences;

    public function __construct()
    {
        $this->cvexperiences = new ArrayCollection();
    }



    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNomentreprise(): ?string
    {
        return $this->nomentreprise;
    }

    public function setNomentreprise(string $nomentreprise): self
    {
        $this->nomentreprise = $nomentreprise;

        return $this;
    }

    public function getPoste(): ?string
    {
        return $this->poste;
    }

    public function setPoste(string $poste): self
    {
        $this->poste = $poste;

        return $this;
    }

    public function getLieu(): ?string
    {
        return $this->lieu;
    }

    public function setLieu(string $lieu): self
    {
        $this->lieu = $lieu;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getDatedebut(): ?\DateTimeInterface
    {
        return $this->datedebut;
    }

    public function setDatedebut(\DateTimeInterface $datedebut): self
    {
        $this->datedebut = $datedebut;

        return $this;
    }

    public function getDatefin(): ?\DateTimeInterface
    {
        return $this->datefin;
    }

    public function setDatefin(\DateTimeInterface $datefin): self
    {
        $this->datefin = $datefin;

        return $this;
    }

    
    /**
     * @return Collection|Cvexperience[]
     */
    public function getCvexperiences(): Collection
    {
        return $this->cvexperiences;
    }

    public function addCvexperience(Cvexperience $cvexperience): self
    {
        if (!$this->cvexperiences->contains($cvexperience)) {
            $this->cvexperiences[] = $cvexperience;
            $cvexperience->setExperience($this);
        }

        return $this;
    }

    public function removeCvexperience(Cvexperience $cvexperience): self
    {
        if ($this->cvexperiences->contains($cvexperience)) {
            $this->cvexperiences->removeElement($cvexperience);
            // set the owning side to null (unless already changed)
            if ($cvexperience->getExperience() === $this) {
                $cvexperience->setExperience(null);
            }
        }

        return $this;
    }

  
}
