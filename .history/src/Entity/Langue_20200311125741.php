<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\LangueRepository")
 */
class Langue
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $libelle;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $niveau;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="langue")
     */
    private $user;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Cv", inversedBy="cv")
     */
    private $cv;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Cv", inversedBy="langue")
     */
    private $parler;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Cv", inversedBy="parler")
     */
    private $langue;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLibelle(): ?string
    {
        return $this->libelle;
    }

    public function setLibelle(string $libelle): self
    {
        $this->libelle = $libelle;

        return $this;
    }

    public function getNiveau(): ?string
    {
        return $this->niveau;
    }

    public function setNiveau(string $niveau): self
    {
        $this->niveau = $niveau;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getCv(): ?Cv
    {
        return $this->cv;
    }

    public function setCv(?Cv $cv): self
    {
        $this->cv = $cv;

        return $this;
    }

    public function getParler(): ?Cv
    {
        return $this->parler;
    }

    public function setParler(?Cv $parler): self
    {
        $this->parler = $parler;

        return $this;
    }

    public function getLangue(): ?Cv
    {
        return $this->langue;
    }

    public function setLangue(?Cv $langue): self
    {
        $this->langue = $langue;

        return $this;
    }
}
