<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\RubriqueRepository")
 */
class Rubrique
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $libel;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Offre", mappedBy="offre")
     */
    private $offres;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Secteur", inversedBy="rubriques")
     */
    private $rubrique;

    public function __construct()
    {
        $this->offres = new ArrayCollection();
        $this->rubrique = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLibel(): ?string
    {
        return $this->libel;
    }

    public function setLibel(string $libel): self
    {
        $this->libelle = $libel;

        return $this;
    }

    /**
     * @return Collection|Offre[]
     */
    
    public function getOffres(): Collection
    {
        return $this->offres;
    }

    public function addOffre(Offre $offre): self
    {
        if (!$this->offres->contains($offre)) {
            $this->offres[] = $offre;
            $offre->setOffre($this);
        }

        return $this;
    }

    public function removeOffre(Offre $offre): self
    {
        if ($this->offres->contains($offre)) {
            $this->offres->removeElement($offre);
            // set the owning side to null (unless already changed)
            if ($offre->getOffre() === $this) {
                $offre->setOffre(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Secteur[]
     */
    public function getRubrique(): Collection
    {
        return $this->rubrique;
    }

    public function addRubrique(Secteur $rubrique): self
    {
        if (!$this->rubrique->contains($rubrique)) {
            $this->rubrique[] = $rubrique;
        }

        return $this;
    }

    public function removeRubrique(Secteur $rubrique): self
    {
        if ($this->rubrique->contains($rubrique)) {
            $this->rubrique->removeElement($rubrique);
        }

        return $this;
    }
}
