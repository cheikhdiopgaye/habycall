<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\EntrepriseRepository")
 */
class Entreprise
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"offreuser"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(message="Le nom de l'entreprise ne doit pas être vide")
     * @Assert\Length(min="2", max="255" ,minMessage="Le nom de l'entreprise est trop court !!")
     * @Groups({"entreprise", "offreuser"})
     */
    private $nomentrep;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"offreuser"})
     */
    private $logo;

    /**
    * @ORM\Column(type="string", length=255)
    * @Assert\NotBlank(message="L'adresse de l'entreprise ne doit pas être vide")
    * @Assert\Length(min="2", max="255" ,minMessage="L'adresse de l'entreprise est trop court !!")
    * @Groups({"entreprise", "offreuser"})
    */
    private $adressentrep;

    /**
    * @ORM\Column(type="string", length=255)
    * @Assert\Length(min="2", max="255" ,minMessage="La description de l'entreprise est trop courte !!")
    * @Groups({"offreuser"})
    */
    private $descriptionentrep;
    /**
    * @ORM\Column(type="string", length=255, nullable=true)
    * @Assert\Length(min="2", max="255" ,minMessage="Le site internet de l'entreprise est trop court !!")
    * @Groups({"entreprise", "offreuser"})})
    */
    private $siteinternet;


    /**
     * @ORM\OneToMany(targetEntity="App\Entity\User", mappedBy="entreprise")
     * @Groups({"entreprise", "offreuser"})})
     */
    private $users;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Secteur", inversedBy="entreprises")
     * @Groups({"offreuser"})
     */
    private $entrsecteur;


    public function __construct()
    {
        $this->users = new ArrayCollection();
        $this->entrsecteur = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNomentrep(): ?string
    {
        return $this->nomentrep;
    }

    public function setNomentrep(string $nomentrep): self
    {
        $this->nomentrep = $nomentrep;

        return $this;
    }

    public function getLogo(): ?string
    {
        return $this->logo;
    }

    public function setLogo(string $logo): self
    {
        $this->logo = $logo;

        return $this;
    }

    public function getAdressentrep(): ?string
    {
        return $this->adressentrep;
    }

    public function setAdressentrep(string $adressentrep): self
    {
        $this->adressentrep = $adressentrep;

        return $this;
    }

    public function getDescriptionentrep(): ?string
    {
        return $this->descriptionentrep;
    }

    public function setDescriptionentrep(string $descriptionentrep): self
    {
        $this->descriptionentrep = $descriptionentrep;

        return $this;
    }

    public function getSiteinternet(): ?string
    {
        return $this->siteinternet;
    }

    public function setSiteinternet(string $siteinternet): self
    {
        $this->siteinternet = $siteinternet;

        return $this;
    }


    /**
     * @return Collection|User[]
     */
    public function getUsers(): Collection
    {
        return $this->users;
    }

    public function addUser(User $user): self
    {
        if (!$this->users->contains($user)) {
            $this->users[] = $user;
            $user->setEntreprise($this);
        }

        return $this;
    }

    public function removeUser(User $user): self
    {
        if ($this->users->contains($user)) {
            $this->users->removeElement($user);
            // set the owning side to null (unless already changed)
            if ($user->getEntreprise() === $this) {
                $user->setEntreprise(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Secteur[]
     */
    public function getEntrsecteur(): Collection
    {
        return $this->entrsecteur;
    }

    public function addEntrsecteur(Secteur $entrsecteur): self
    {
        if (!$this->entrsecteur->contains($entrsecteur)) {
            $this->entrsecteur[] = $entrsecteur;
        }

        return $this;
    }

    public function removeEntrsecteur(Secteur $entrsecteur): self
    {
        if ($this->entrsecteur->contains($entrsecteur)) {
            $this->entrsecteur->removeElement($entrsecteur);
        }

        return $this;
    }


}
