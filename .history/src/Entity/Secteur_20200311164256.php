<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\SecteurRepository")
 */
class Secteur
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $libellee;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Rubrique", mappedBy="rubrique")
     */
    private $rubriques;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Metier", mappedBy="secteur")
     */
    private $secteur;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Entreprise", mappedBy="avoir")
     */
    private $entreprises;

    public function __construct()
    {
        $this->rubriques = new ArrayCollection();
        $this->secteur = new ArrayCollection();
        $this->entreprises = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLibellee(): ?string
    {
        return $this->libellee;
    }

    public function setLibellee(string $libellee): self
    {
        $this->libelle = $libellee;

        return $this;
    }

    /**
     * @return Collection|Rubrique[]
     */
    
    public function getRubriques(): Collection
    {
        return $this->rubriques;
    }

    public function addRubrique(Rubrique $rubrique): self
    {
        if (!$this->rubriques->contains($rubrique)) {
            $this->rubriques[] = $rubrique;
            $rubrique->addRubrique($this);
        }

        return $this;
    }

    public function removeRubrique(Rubrique $rubrique): self
    {
        if ($this->rubriques->contains($rubrique)) {
            $this->rubriques->removeElement($rubrique);
            $rubrique->removeRubrique($this);
        }

        return $this;
    }

    /**
     * @return Collection|Metier[]
     */
    public function getSecteur(): Collection
    {
        return $this->secteur;
    }

    public function addSecteur(Metier $secteur): self
    {
        if (!$this->secteur->contains($secteur)) {
            $this->secteur[] = $secteur;
            $secteur->setSecteur($this);
        }

        return $this;
    }

    public function removeSecteur(Metier $secteur): self
    {
        if ($this->secteur->contains($secteur)) {
            $this->secteur->removeElement($secteur);
            // set the owning side to null (unless already changed)
            if ($secteur->getSecteur() === $this) {
                $secteur->setSecteur(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Entreprise[]
     */
    public function getEntreprises(): Collection
    {
        return $this->entreprises;
    }

    public function addEntreprise(Entreprise $entreprise): self
    {
        if (!$this->entreprises->contains($entreprise)) {
            $this->entreprises[] = $entreprise;
            $entreprise->addAvoir($this);
        }

        return $this;
    }

    public function removeEntreprise(Entreprise $entreprise): self
    {
        if ($this->entreprises->contains($entreprise)) {
            $this->entreprises->removeElement($entreprise);
            $entreprise->removeAvoir($this);
        }

        return $this;
    }
}
