<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert; //pour la validation des données

/**
 * @ORM\Entity(repositoryClass="App\Repository\EntrepriseRepository")
 */
class Entreprise
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(message="Le nom de l'entreprise ne doit pas être vide")
     * @Assert\Length(min="2", max="255" ,minMessage="Le nom de l'entreprise est trop court !!")
     * @Groups({"entreprise"})
     */
    private $nomentrep;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $logo;

    /**
    * @ORM\Column(type="string", length=255)
    * @Assert\NotBlank(message="L'adresse de l'entreprise ne doit pas être vide")
    * @Assert\Length(min="2", max="255" ,minMessage="L'adresse de l'entreprise est trop court !!")
    * @Groups({"entreprise"})
    */
    private $adressentrep;

    /**
    * @ORM\Column(type="string", length=255)
    * @Assert\Length(min="2", max="255" ,minMessage="La description de l'entreprise est trop courte !!")
    */
    private $descriptionentrep;
    /**
    * @ORM\Column(type="string", length=255, nullable=true)
    * @Assert\Length(min="2", max="255" ,minMessage="Le site internet de l'entreprise est trop court !!")
    * @Groups({"entreprise"})
    */
    private $siteinternet;

    /**
    * @ORM\Column(type="string", length=255)
    * @Assert\NotBlank(message="Le secteur d'activite de l'entreprise ne doit pas être vide")
    * @Assert\Length(min="2", max="255" ,minMessage="Le secteur d'activite de l'entreprise est trop court !!")
    * @Groups({"entreprise"})
    */
    private $secteuractivite;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\User", mappedBy="entreprise")
     * @Groups({"entreprise"})
     */
    private $users;

    public function __construct()
    {
        $this->users = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNomentrep(): ?string
    {
        return $this->nomentrep;
    }

    public function setNomentrep(string $nomentrep): self
    {
        $this->nomentrep = $nomentrep;

        return $this;
    }

    public function getLogo(): ?string
    {
        return $this->logo;
    }

    public function setLogo(string $logo): self
    {
        $this->logo = $logo;

        return $this;
    }

    public function getAdressentrep(): ?string
    {
        return $this->adressentrep;
    }

    public function setAdressentrep(string $adressentrep): self
    {
        $this->adressentrep = $adressentrep;

        return $this;
    }

    public function getDescriptionentrep(): ?string
    {
        return $this->descriptionentrep;
    }

    public function setDescriptionentrep(string $descriptionentrep): self
    {
        $this->descriptionentrep = $descriptionentrep;

        return $this;
    }

    public function getSiteinternet(): ?string
    {
        return $this->siteinternet;
    }

    public function setSiteinternet(string $siteinternet): self
    {
        $this->siteinternet = $siteinternet;

        return $this;
    }

    public function getSecteuractivite(): ?string
    {
        return $this->secteuractivite;
    }

    public function setSecteuractivite(string $secteuractivite): self
    {
        $this->secteuractivite = $secteuractivite;

        return $this;
    }

    /**
     * @return Collection|User[]
     */
    public function getUsers(): Collection
    {
        return $this->users;
    }

    public function addUser(User $user): self
    {
        if (!$this->users->contains($user)) {
            $this->users[] = $user;
            $user->setEntreprise($this);
        }

        return $this;
    }

    public function removeUser(User $user): self
    {
        if ($this->users->contains($user)) {
            $this->users->removeElement($user);
            // set the owning side to null (unless already changed)
            if ($user->getEntreprise() === $this) {
                $user->setEntreprise(null);
            }
        }

        return $this;
    }
}
