<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\MetierRepository")
 */
class Metier
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $libelle;


    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Secteur", inversedBy="metiers")
     */
    private $metiersecteur;


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLibelle(): ?string
    {
        return $this->libelle;
    }

    public function setLibelle(string $libelle): self
    {
        $this->libelle = $libelle;

        return $this;
    }

    public function getMetiersecteur(): ?Secteur
    {
        return $this->metiersecteur;
    }

    public function setMetiersecteur(?Secteur $metiersecteur): self
    {
        $this->metiersecteur = $metiersecteur;

        return $this;
    }

}
