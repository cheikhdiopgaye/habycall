<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass="App\Repository\FormationRepository")
 */
class Formation
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"offreuser"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"offreuser"})
     */
    private $diplome;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"offreuser"})
     */
    private $etablissement;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"offreuser"})
     */
    private $description;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"offreuser"})
     */
    private $domaine;

    /**
     * @ORM\Column(type="date")
     * @Groups({"offreuser"})
     */
    private $datedebut;

    /**
     * @ORM\Column(type="date")
     * @Groups({"offreuser"})
     */
    private $datefin;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Cv", mappedBy="formation")
     * @Groups({"offreuser"})
     */
    private $cvs;

    public function __construct()
    {
        $this->cvs = new ArrayCollection();
    }


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDiplome(): ?string
    {
        return $this->diplome;
    }

    public function setDiplome(string $diplome): self
    {
        $this->diplome = $diplome;

        return $this;
    }

    public function getEtablissement(): ?string
    {
        return $this->etablissement;
    }

    public function setEtablissement(string $etablissement): self
    {
        $this->etablissement = $etablissement;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getDomaine(): ?string
    {
        return $this->domaine;
    }

    public function setDomaine(string $domaine): self
    {
        $this->domaine = $domaine;

        return $this;
    }

    public function getDatedebut(): ?\DateTimeInterface
    {
        return $this->datedebut;
    }

    public function setDatedebut(\DateTimeInterface $datedebut): self
    {
        $this->datedebut = $datedebut;

        return $this;
    }

    public function getDatefin(): ?\DateTimeInterface
    {
        return $this->datefin;
    }

    public function setDatefin(\DateTimeInterface $datefin): self
    {
        $this->datefin = $datefin;

        return $this;
    }

    /**
     * @return Collection|Cv[]
     */
    public function getCvs(): Collection
    {
        return $this->cvs;
    }

    public function addCv(Cv $cv): self
    {
        if (!$this->cvs->contains($cv)) {
            $this->cvs[] = $cv;
            $cv->setFormation($this);
        }

        return $this;
    }

    public function removeCv(Cv $cv): self
    {
        if ($this->cvs->contains($cv)) {
            $this->cvs->removeElement($cv);
            // set the owning side to null (unless already changed)
            if ($cv->getFormation() === $this) {
                $cv->setFormation(null);
            }
        }

        return $this;
    }

}
