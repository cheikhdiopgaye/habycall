<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CvRepository")
 */
class Cv
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="date")
     */
    private $datecreation;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $statut;


    /**
     * @ORM\Column(type="string", length=255)
     */
    private $titre;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Infocandidat", cascade={"persist", "remove"})
     */
    private $cvinfo;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\User", cascade={"persist", "remove"})
     */
    private $cvuser;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Formation", inversedBy="cvs")
     */
    private $formation;


    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Langue", inversedBy="cvs")
     */
    private $parle;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Experience", inversedBy="cvs")
     */
    private $experience;

 

 


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDatecreation(): ?\DateTimeInterface
    {
        return $this->datecreation;
    }

    public function setDatecreation(\DateTimeInterface $datecreation): self
    {
        $this->datecreation = $datecreation;

        return $this;
    }

    public function getStatut(): ?string
    {
        return $this->statut;
    }

    public function setStatut(string $statut): self
    {
        $this->statut = $statut;

        return $this;
    }


    public function getTitre(): ?string
    {
        return $this->titre;
    }

    public function setTitre(string $titre): self
    {
        $this->titre = $titre;

        return $this;
    }

    public function getCvinfo(): ?Infocandidat
    {
        return $this->cvinfo;
    }

    public function setCvinfo(?Infocandidat $cvinfo): self
    {
        $this->cvinfo = $cvinfo;

        return $this;
    }

    public function getCvuser(): ?User
    {
        return $this->cvuser;
    }

    public function setCvuser(?User $cvuser): self
    {
        $this->cvuser = $cvuser;

        return $this;
    }

    public function getFormation(): ?Formation
    {
        return $this->formation;
    }

    public function setFormation(?Formation $formation): self
    {
        $this->formation = $formation;

        return $this;
    }

    public function getParle(): ?Langue
    {
        return $this->parle;
    }

    public function setParle(?Langue $parle): self
    {
        $this->parle = $parle;

        return $this;
    }

    public function getExperience(): ?Experience
    {
        return $this->experience;
    }

    public function setExperience(?Experience $experience): self
    {
        $this->experience = $experience;

        return $this;
    }
  
}
