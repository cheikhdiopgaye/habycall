<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CvformationRepository")
 */
class Cvformation
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"offreuser"})
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Cv", inversedBy="cvformations")
     * @Groups({"offreuser"})
     */
    private $cv;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Formation", inversedBy="cvformations")
     * @Groups({"offreuser"})
     */
    private $formation;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCv(): ?Cv
    {
        return $this->cv;
    }

    public function setCv(?Cv $cv): self
    {
        $this->cv = $cv;

        return $this;
    }

    public function getFormation(): ?Formation
    {
        return $this->formation;
    }

    public function setFormation(?Formation $formation): self
    {
        $this->formation = $formation;

        return $this;
    }
}
