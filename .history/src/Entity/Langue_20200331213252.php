<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass="App\Repository\LangueRepository")
 */
class Langue
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"offreuser"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"offreuser"})
     */
    private $libeller;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"offreuser"})
     */
    private $niveau;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Cvlangue", mappedBy="langue")
     * @Groups({"offreuser"})
     */
    private $cvlangues;

    public function __construct()
    {
        $this->cvlangues = new ArrayCollection();
    }


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLibeller(): ?string
    {
        return $this->libeller;
    }

    public function setLibeller(string $libeller): self
    {
        $this->libeller = $libeller;

        return $this;
    }

    public function getNiveau(): ?string
    {
        return $this->niveau;
    }

    public function setNiveau(string $niveau): self
    {
        $this->niveau = $niveau;

        return $this;
    }

    /**
     * @return Collection|Cvlangue[]
     */
    public function getCvlangues(): Collection
    {
        return $this->cvlangues;
    }

    public function addCvlangue(Cvlangue $cvlangue): self
    {
        if (!$this->cvlangues->contains($cvlangue)) {
            $this->cvlangues[] = $cvlangue;
            $cvlangue->setLangue($this);
        }

        return $this;
    }

    public function removeCvlangue(Cvlangue $cvlangue): self
    {
        if ($this->cvlangues->contains($cvlangue)) {
            $this->cvlangues->removeElement($cvlangue);
            // set the owning side to null (unless already changed)
            if ($cvlangue->getLangue() === $this) {
                $cvlangue->setLangue(null);
            }
        }

        return $this;
    }


}
