<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CvRepository")
 */
class Cv
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"offreuser"})
     */
    private $id;

    /**
     * @ORM\Column(type="date")
     * @Groups({"offreuser"})
     */
    private $datecreation;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"offreuser"})
     */
    private $statut;


    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"offreuser"})
     */
    private $titre;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Infocandidat", inversedBy="cvs")
     * @Groups({"offreuser"})

     */
    private $infocandidat;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Video", inversedBy="cvs")
     * @Groups({"offreuser"})
     */
    private $video;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Cvlangue", mappedBy="cv")
     * @Groups({"offreuser"})
     */
    private $cvlangues;

    public function __construct()
    {
        $this->cvlangues = new ArrayCollection();
    }


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDatecreation(): ?\DateTimeInterface
    {
        return $this->datecreation;
    }

    public function setDatecreation(\DateTimeInterface $datecreation): self
    {
        $this->datecreation = $datecreation;

        return $this;
    }

    public function getStatut(): ?string
    {
        return $this->statut;
    }

    public function setStatut(string $statut): self
    {
        $this->statut = $statut;

        return $this;
    }


    public function getTitre(): ?string
    {
        return $this->titre;
    }

    public function setTitre(string $titre): self
    {
        $this->titre = $titre;

        return $this;
    }

    public function getInfocandidat(): ?Infocandidat
    {
        return $this->infocandidat;
    }

    public function setInfocandidat(?Infocandidat $infocandidat): self
    {
        $this->infocandidat = $infocandidat;

        return $this;
    }

    public function getVideo(): ?Video
    {
        return $this->video;
    }

    public function setVideo(?Video $video): self
    {
        $this->video = $video;

        return $this;
    }

    /**
     * @return Collection|Cvlangue[]
     */
    public function getCvlangues(): Collection
    {
        return $this->cvlangues;
    }

    public function addCvlangue(Cvlangue $cvlangue): self
    {
        if (!$this->cvlangues->contains($cvlangue)) {
            $this->cvlangues[] = $cvlangue;
            $cvlangue->setCv($this);
        }

        return $this;
    }

    public function removeCvlangue(Cvlangue $cvlangue): self
    {
        if ($this->cvlangues->contains($cvlangue)) {
            $this->cvlangues->removeElement($cvlangue);
            // set the owning side to null (unless already changed)
            if ($cvlangue->getCv() === $this) {
                $cvlangue->setCv(null);
            }
        }

        return $this;
    }
  
}
