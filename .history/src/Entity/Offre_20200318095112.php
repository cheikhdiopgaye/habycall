<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert; //pour la validation des données

/**
 * @ORM\Entity(repositoryClass="App\Repository\OffreRepository")
 */
class Offre
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="date")
     * @Groups({"offre"})
     */
    private $datedebut;

    /**
     * @ORM\Column(type="date")
     * @Groups({"offre"})
     */
    private $datefin;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"offre"})
     * @Assert\NotBlank(message="Le type de contrat ne doit pas être vide")
     * @Assert\Length(min="2", max="255" ,minMessage="Le type de contrat est trop court !!")
     */
    private $typecontrat;


    /**
    * @ORM\Column(type="string", length=255)
    * @Groups({"offre"})
    * @Assert\NotBlank(message="La description ne doit pas être vide")
    * @Assert\Length(min="2", max="255" ,minMessage="La description est trop courte !!")
    */
    private $description;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Cv", mappedBy="cvoffre")
     */
    private $cvoffre;



    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Rubrique", inversedBy="offres")
     */
    private $offrerubrique;

    public function __construct()
    {
        $this->cvoffre = new ArrayCollection();
        $this->offremetier = new ArrayCollection();
    }


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDatedebut(): ?\DateTimeInterface
    {
        return $this->datedebut;
    }

    public function setDatedebut(\DateTimeInterface $datedebut): self
    {
        $this->datedebut = $datedebut;

        return $this;
    }

    public function getDatefin(): ?\DateTimeInterface
    {
        return $this->datefin;
    }

    public function setDatefin(\DateTimeInterface $datefin): self
    {
        $this->datefin = $datefin;

        return $this;
    }

    public function getTypecontrat(): ?string
    {
        return $this->typecontrat;
    }

    public function setTypecontrat(string $typecontrat): self
    {
        $this->typecontrat = $typecontrat;

        return $this;
    }


    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return Collection|Cv[]
     */
    public function getCvoffre(): Collection
    {
        return $this->cvoffre;
    }

    public function addCvoffre(Cv $cvoffre): self
    {
        if (!$this->cvoffre->contains($cvoffre)) {
            $this->cvoffre[] = $cvoffre;
            $cvoffre->addCvoffre($this);
        }

        return $this;
    }

    public function removeCvoffre(Cv $cvoffre): self
    {
        if ($this->cvoffre->contains($cvoffre)) {
            $this->cvoffre->removeElement($cvoffre);
            $cvoffre->removeCvoffre($this);
        }

        return $this;
    }

    /**
     * @return Collection|Metier[]
     */
    public function getOffremetier(): Collection
    {
        return $this->offremetier;
    }

    public function addOffremetier(Metier $offremetier): self
    {
        if (!$this->offremetier->contains($offremetier)) {
            $this->offremetier[] = $offremetier;
            $offremetier->setOffre($this);
        }

        return $this;
    }

    public function removeOffremetier(Metier $offremetier): self
    {
        if ($this->offremetier->contains($offremetier)) {
            $this->offremetier->removeElement($offremetier);
            // set the owning side to null (unless already changed)
            if ($offremetier->getOffre() === $this) {
                $offremetier->setOffre(null);
            }
        }

        return $this;
    }

    public function getOffrerubrique(): ?Rubrique
    {
        return $this->offrerubrique;
    }

    public function setOffrerubrique(?Rubrique $offrerubrique): self
    {
        $this->offrerubrique = $offrerubrique;

        return $this;
    }



}
