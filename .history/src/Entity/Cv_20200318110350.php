<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CvRepository")
 */
class Cv
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="date")
     */
    private $datecreation;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $statut;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Langue", mappedBy="langue")
     */
    private $parler;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Formation", mappedBy="former")
     */
    private $formations;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Experience", mappedBy="experimenter")
     */
    private $experiences;


    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Offre", inversedBy="cvoffre")
     */
    private $cvoffre;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $titre;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Infocandidat", cascade={"persist", "remove"})
     */
    private $cvinfo;

    public function __construct()
    {
        $this->parler = new ArrayCollection();
        $this->formations = new ArrayCollection();
        $this->experiences = new ArrayCollection();
        $this->cvoffre = new ArrayCollection();
    }

 


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDatecreation(): ?\DateTimeInterface
    {
        return $this->datecreation;
    }

    public function setDatecreation(\DateTimeInterface $datecreation): self
    {
        $this->datecreation = $datecreation;

        return $this;
    }

    public function getStatut(): ?string
    {
        return $this->statut;
    }

    public function setStatut(string $statut): self
    {
        $this->statut = $statut;

        return $this;
    }

    /**
     * @return Collection|Langue[]
     */
    public function getParler(): Collection
    {
        return $this->parler;
    }

    public function addParler(Langue $parler): self
    {
        if (!$this->parler->contains($parler)) {
            $this->parler[] = $parler;
            $parler->setLangue($this);
        }

        return $this;
    }

    public function removeParler(Langue $parler): self
    {
        if ($this->parler->contains($parler)) {
            $this->parler->removeElement($parler);
            // set the owning side to null (unless already changed)
            if ($parler->getLangue() === $this) {
                $parler->setLangue(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Formation[]
     */
    public function getFormations(): Collection
    {
        return $this->formations;
    }

    public function addFormation(Formation $formation): self
    {
        if (!$this->formations->contains($formation)) {
            $this->formations[] = $formation;
            $formation->setFormer($this);
        }

        return $this;
    }

    public function removeFormation(Formation $formation): self
    {
        if ($this->formations->contains($formation)) {
            $this->formations->removeElement($formation);
            // set the owning side to null (unless already changed)
            if ($formation->getFormer() === $this) {
                $formation->setFormer(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Experience[]
     */
    public function getExperiences(): Collection
    {
        return $this->experiences;
    }

    public function addExperience(Experience $experience): self
    {
        if (!$this->experiences->contains($experience)) {
            $this->experiences[] = $experience;
            $experience->setExperimenter($this);
        }

        return $this;
    }

    public function removeExperience(Experience $experience): self
    {
        if ($this->experiences->contains($experience)) {
            $this->experiences->removeElement($experience);
            // set the owning side to null (unless already changed)
            if ($experience->getExperimenter() === $this) {
                $experience->setExperimenter(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Offre[]
     */
    public function getCvoffre(): Collection
    {
        return $this->cvoffre;
    }

    public function addCvoffre(Offre $cvoffre): self
    {
        if (!$this->cvoffre->contains($cvoffre)) {
            $this->cvoffre[] = $cvoffre;
        }

        return $this;
    }

    public function removeCvoffre(Offre $cvoffre): self
    {
        if ($this->cvoffre->contains($cvoffre)) {
            $this->cvoffre->removeElement($cvoffre);
        }

        return $this;
    }

    public function getTitre(): ?string
    {
        return $this->titre;
    }

    public function setTitre(string $titre): self
    {
        $this->titre = $titre;

        return $this;
    }

    public function getCvinfo(): ?Infocandidat
    {
        return $this->cvinfo;
    }

    public function setCvinfo(?Infocandidat $cvinfo): self
    {
        $this->cvinfo = $cvinfo;

        return $this;
    }

  
}
