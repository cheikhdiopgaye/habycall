<?php

namespace App\Repository;

use App\Entity\User;
use App\Entity\Entreprise;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;

/**
 * @method User|null find($id, $lockMode = null, $lockVersion = null)
 * @method User|null findOneBy(array $criteria, array $orderBy = null)
 * @method User[]    findAll()
 * @method User[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, User::class);
    }
         /**
     * @return Utilisateur[] Returns an array of Utilisateur objects
     */

    public function findUserEntreprise(Entreprise $entreprise,User $user)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.entreprise = :val')
            ->andWhere('u != :val2')
            ->setParameter('val', $entreprise)
            ->setParameter('val2', $user)
            ->getQuery()
            ->getResult()
        ;
    }
    
    public function findCandidat(User $user): ?User
    {   

        return $this->createQueryBuilder('u')
            ->andWhere('u.user = :val')
            ->andWhere('u.roles LIKE :role')
            ->setParameter('val', $user)
            ->setParameter('role', '%"'.'ROLE_CANDIDAT'.'"%')
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult()
        ;
        //le premier sera toujour l afmin principal car c est le premier à etre ajouter donc il est le premier à avoir id de l entreprise
    }

    // /**
    //  * @return User[] Returns an array of User objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('u.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?User
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}


