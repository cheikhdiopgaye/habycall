<?php

namespace App\Service;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class MailerService extends AbstractController
{
  private $mailer;
  public function __construct(\Swift_Mailer $mailer)
  {
      $this->mailer=$mailer;
  }

  public function mailer($subject, $from = null, $to, $message){
    if (empty($to)) {
      return false;
    }
    $from = empty($from) ? ['badianeelina8@gmail.com'=> 'Administrateur'] : $from;
    $email = (new \Swift_Message($subject))
    ->setFrom($from)
    ->setBody($message, "text/plain")
    ->setTo($to);
    $headers = $email->getHeaders();
    $headers->addTextHeader('Content-Type', 'text/html; charset=utf-8');
    $response = $this->mailer->send($email);
    return $response;
    }
}