<?php

namespace App\Controller;

use App\Entity\User;
use App\Service\MailerService;
use App\Entity\Wallet;
use App\Form\UserType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormError;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Csrf\TokenGenerator\TokenGeneratorInterface;

/**
 *  @Route("/api")
 */
class SecurityController extends AbstractController
{
    private $message;

    public function __construct(MailerService  $mailer)
    {
        $this->mailer = $mailer;
        $this->message = 'message';
    }
    /**
      * @Route("/logincheck", name="login", methods={"POST","GET"})
      */
    public function login(Request $request)
    {
        $user = $this->getUser();

        return $this->json([
            'username' => $user->getUsername(),
            'roles' => $user->getRoles(),
        ]);
    } 

/**
 * @Route("/resetting", name="resetting", methods="GET|POST")
 */
public function resetPassword(Request $request, UserPasswordEncoderInterface $passwordEncoder, \Swift_Mailer $mailer): Response
{
          $entityManager = $this->getDoctrine()->getManager();
          $user=$this->getUser();
          $form=$this->createForm(UserType::class, $user);
          $form->handleRequest($request);
             if($form->isSubmitted() &&$form->isValid()){
              $username = $request->query->get('username');
              if ($request->isMethod('GET') && is_null($username)) {
                $this->addFlash('danger', 'Utilisateur non reconnu');
                return $this->redirectToRoute('login');
              }  
              if ($request->isMethod('POST')) { 
                $username = $request->request->get('username');          
              $passwordEncoder = $this->get('security.password_encoder');
              $password = $request->request->get('password');
              $confirmepassword = $request->request->get('confirmepassword');
               
              //Si l'ancien mot de passe est bon

               if ($passwordEncoder->isPasswordValid($user, $password)&& $passwordEncoder->isPasswordValid($user, $confirmepassword)) {
                  $newEncodedPassword=$passwordEncoder->encoderPassword($user, $user->getConfirmepassword() );
                  $user->setPassword($newEncodedPassword);
                  $user->setConfirmepassword($newEncodedPassword);
                  $message= 'Votre mot de passe est maintenant modifié.';
                  $response = $this->mailer->mailer('Bienvenue dans l\'application Ujob votre Mot de passe oublié - Réinisialisation','',$user->getUsername(),$message);
                
                $entityManager->persist($user); 
                $entityManager->flush();
                $this->addFlash('notice', 'Mot de passe mis à jour !');
                return $this->redirectToRoute('login');
            }else {
                $form->addError(new FormError('Ancien mot de passe incorrect'));
            }
               }
             }
            }
            
}