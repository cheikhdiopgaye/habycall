<?php

namespace App\Controller;


use App\Entity\Cv;
use App\Entity\User;
use App\Form\CvType;
use App\Entity\Offre;
use App\Entity\Video;
use App\Entity\Langue;
use App\Entity\Metier;
use App\Entity\Secteur;
use App\Form\OffreType;
use App\Form\VideoType;
use App\Entity\Rubrique;
use App\Form\LangueType;
use App\Form\MetierType;
use App\Entity\Formation;
use App\Form\SecteurType;
use App\Entity\Experience;
use App\Form\RubriqueType;
use App\Form\FormationType;
use App\Form\ExperienceType;
use App\Repository\UserRepository;
use App\Repository\OffreRepository;
use App\Repository\UseroffreRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Entity\Useroffre;

/** 
* @Route("/api")
*/
class OffreController extends FOSRestController
{
    private $correcte;

    public function __construct()
    {
        $this-> correcte="Correcte";
    }

    /**
     * @Route("/security/postuler", name="postuler", methods={"POST"})
     * @IsGranted({"ROLE_CANDIDAT"}, statusCode=403, message="Vous n'avez pas accès à cette page !")
     */
    public function postuler(Request $request, EntityManagerInterface $entityManager, SerializerInterface $serializer,ValidatorInterface $validator)
    {
       $offre = new Offre();

       $connecte=$this->getUser();
       //$useroffre = $this->getDoctrine()->getRepository(Useroffre::class)->find($connecte);;
       //$offre->addUseroffre($useroffre); 
       $form=$this->createForm(OffreType::class , $offre);
       $form->handleRequest($request);
       $data=$request->request->all();
        $form->submit($data);
       // dd($data);
        $entityManager = $this->getDoctrine()->getManager();
        $useroffre = new Useroffre();
        $useroffre->setUser($connecte);
        $useroffre->setOffre($offre);
        $offre->addUseroffre($useroffre); 
        //dd($useroffre);
        $entityManager->persist($useroffre);
        $entityManager->persist($offre);
        $entityManager->flush();
    return new Response('L/annonce a été bien ajouté ',Response::HTTP_CREATED); 
    }

/**
 * @Route("/security/lister/annonces", name="offre_annonceur", methods={"GET"})
 * @IsGranted({"ROLE_SUPERADMIN"}, statusCode=403, message="Vous n'avez pas accès à cette page !") */
public function ListerOffre(OffreRepository $offreRepository, SerializerInterface $serializer)
{
    $par = $offreRepository->findAll();
    $users = $serializer->serialize($par, 'json', ['groups' => ['offre']]);
    return new Response($users, 200, [
        'Content-Type' => 'application/json'
    ]);
}

/**
 * @Route("/security/parcandidat", name="listecandidat", methods={"GET"})
 * @IsGranted({"ROLE_ANNONCEUR"}, statusCode=403, message="Vous n'avez pas accès à cette page !" )
 */

public function listpostule(UseroffreRepository $useroffreRepo, SerializerInterface $serializer,EntityManagerInterface $entityManager)
{
    $connecte = $this->getUser();
    $offre=$useroffreRepo->findBy(['user'=>$connecte]);
    $data=$serializer->serialize($offre, 'json', ['groups' => ['offreuser']]);
    return new Response($data, 200, [
        'content_Type' => 'application/json'
    ]);

}

/**
* @Route("/security/cv", name="cv", methods={"POST"})
* @IsGranted({"ROLE_CANDIDAT"}, statusCode=403, message="Vous n'avez pas accès à cette page !")
*/

public function ajoutcv (Request $request, EntityManagerInterface $entityManager, SerializerInterface $serializer,ValidatorInterface $validator)
    {
        $cv = new Cv(); 

        $form=$this->createForm(CvType::class , $cv);
        $form->handleRequest($request);
        $data=$request->request->all();
         $form->submit($data);
         $cv->setStatut('Correcte');
         $entityManager = $this->getDoctrine()->getManager();
         $errors = $validator->validate($cv);
         if(count($errors)) {
             $errors = $serializer->serialize($errors, 'json');
             return new Response($errors, 500, [
                 'Content-Typle' => 'applicatlion/json'
             ]);
         } 
         $connecte=$this->getUser();
         $user = $this->getDoctrine()->getRepository(User::class)->find($connecte);
          $cv->setCvuser($user);

         $entityManager->persist($cv);
              //  dd($cv);

         $entityManager->flush();
         return new Response('Le Cv a bien été ajouteé',Response::HTTP_CREATED); 
        }



/**
* @Route("/security/experience", name="experience", methods={"POST"})
* @IsGranted({"ROLE_CANDIDAT"}, statusCode=403, message="Vous n'avez pas accès à cette page !")
*/

public function ajoutexperience (Request $request, EntityManagerInterface $entityManager, SerializerInterface $serializer,ValidatorInterface $validator)
    {
         $experience = new Experience();
        $connecte=$this->getUser()->getCvuser();
        $user = $this->getDoctrine()->getRepository(User::class)->find($connecte);
        //$experience->addCv($cv);
        dd($connecte);

         $form=$this->createForm(ExperienceType::class , $experience);
         $form->handleRequest($request);
         $data=$request->request->all();
          $form->submit($data);
          $entityManager = $this->getDoctrine()->getManager();
          $errors = $validator->validate($experience);
          if(count($errors)) {
              $errors = $serializer->serialize($errors, 'json');
              return new Response($errors, 500, [
                  'Content-Typle' => 'applicatlion/json'
              ]);
          } 
         $entityManager->persist($experience);
         $entityManager->flush();
        return new Response('Le Candidat a bien ajouté ses expériences',Response::HTTP_CREATED); 
    }


/**
 * @Route("/security/annonce", name="annonce", methods={"POST"})
 * @IsGranted({"ROLE_ANNONCEUR"}, statusCode=403, message="Vous n'avez pas accès à cette page !" )
 */

public function faireannonce (Request $request, EntityManagerInterface $entityManager, SerializerInterface $serializer,ValidatorInterface $validator)
{
    $offre = new Offre();
    $connecte=$this->getUser();
    $user = $this->getDoctrine()->getRepository(User::class)->find($connecte);
    $offre->setUser($user); 
    $form=$this->createForm(OffreType::class , $offre);
    $form->handleRequest($request);
    $data=$request->request->all();
     $form->submit($data);
    // dd($data);
     $entityManager = $this->getDoctrine()->getManager();

     $rubrique =new Rubrique();
     $form=$this->createForm(RubriqueType::class , $rubrique);
     $form->submit($data);

     $entityManager = $this->getDoctrine()->getManager();
     $errors = $validator->validate($rubrique);
     if(count($errors)) {
         $errors = $serializer->serialize($errors, 'json');
         return new Response($errors, 500, [
             'Content-Typle' => 'applicatlion/json'
         ]);
     } 
     $rubrique->addOffre($offre);

     $metier =  new Metier();
     $form=$this->createForm(MetierType::class , $metier);
     $form->submit($data);

     $entityManager = $this->getDoctrine()->getManager();
     $errors = $validator->validate($metier);
     if(count($errors)) {
         $errors = $serializer->serialize($errors, 'json');
         return new Response($errors, 500, [
             'Content-Typle' => 'applicatlion/json'
         ]);
     } 
     $metier->addOffre($offre);

     $secteur = new Secteur();
     $form=$this->createForm(SecteurType::class , $secteur);
     $form->submit($data);

     $entityManager = $this->getDoctrine()->getManager();
     $errors = $validator->validate($secteur);
     if(count($errors)) {
         $errors = $serializer->serialize($errors, 'json');
         return new Response($errors, 500, [
             'Content-Typle' => 'applicatlion/json'
         ]);
     } 
     $metier->setMetiersecteur($secteur);

     $entityManager->persist($rubrique);
     $entityManager->persist($secteur);
     $entityManager->persist($metier); 
     //dd($useroffre);
     $entityManager->persist($offre);
     $entityManager->flush();
 return new Response('L/annonce a été bien ajouté ',Response::HTTP_CREATED); 

}

/**
 * @Route("/security/parannonceur", name="parannonceur", methods={"GET"})
 * @IsGranted({"ROLE_ANNONCEUR"}, statusCode=403, message="Vous n'avez pas accès à cette page !" )
 */

public function listeposter(OffreRepository $offreRepo, SerializerInterface $serializer,EntityManagerInterface $entityManager)
{
    $connecte = $this->getUser();
    $offre=$offreRepo->findBy(['user'=>$connecte]);
    $data=$serializer->serialize($offre, 'json', ['groups' => ['offreuser']]);
    return new Response($data, 200, [
        'content_Type' => 'application/json'
    ]);

}
}
