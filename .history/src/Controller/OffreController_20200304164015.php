<?php

namespace App\Controller;

use App\Entity\User;
use App\Entity\Offre;
use App\Form\OffreType;
use App\Repository\UserRepository;
use App\Repository\OffreRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/** 
* @Route("/api")
*/
class OffreController extends FOSRestController
{
    /**
     * @Route("/security/offre", name="offre", methods={"POST"})
     * @IsGranted({"ROLE_ANNONCEUR"}, statusCode=403, message="Vous n'avez pas accès à cette page !")
     */
    public function faireannonce(Request $request, EntityManagerInterface $entityManager)
    {
       $offre = new Offre();
       $connecte=$this->getUser();
       $user = $this->getDoctrine()->getRepository(User::class)->find($connecte);
       $offre->setUser($user);
       $form=$this->createForm(OffreType::class , $offre);
       $form->handleRequest($request);
       $data=$request->request->all();
        $form->submit($data);
        $offre->setDatedebut(new \Datetime('now'));
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($offre);
        $entityManager->flush();
    return new Response('L/annonce a été bien ajouté ',Response::HTTP_CREATED); 
    }

/**
 * @Route("/security/lister/annonces", name="offre_annonceur", methods={"GET"})
 * @IsGranted({"ROLE_SUPERADMIN"}, statusCode=403, message="Vous n'avez pas accès à cette page !") */
public function ListerOffre(OffreRepository $offreRepository, SerializerInterface $serializer)
{
    $par = $offreRepository->findAll();
    $users = $serializer->serialize($par, 'json', ['groups' => ['offre']]);
    return new Response($users, 200, [
        'Content-Type' => 'application/json'
    ]);
}

/**
 * @Route("/security/lister/parannonceur/{id}", name="lister_annonceur", methods={"GET"})
 * @IsGranted({"ROLE_ANNONCEUR"}, statusCode=403, message="Vous n'avez pas accès à cette page !" )
 */

public function OffreparAnnonceur(UserRepository $userRepository, SerializerInterface $serializer)

{  
   $connecte = $this->getUser();
   $offre=$userRepository->findBy(['offre'=>$connecte ]);
   $data=$serializer->serialize($offre, 'json', ['groups' => ['offre']]);
   return new Response($data, 200, [
       'content_Type' => 'application/json'
   ]);
}

     /**
     * @Route("/security/candidater", name="postule", methods={"POST"})
     * @IsGranted({"ROLE_CANDIDAT"}, statusCode=403, message="Vous n'avez pas accès à cette page !")
     */

    public function postuler (Request $request, EntityManagerInterface $entityManager)
    {
        return new Response('Le Candidat a bien postulé',Response::HTTP_CREATED); 
    }
}
