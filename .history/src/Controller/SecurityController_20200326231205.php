<?php

namespace App\Controller;

use App\Entity\User;
use App\Service\MailerService;
use App\Entity\Wallet;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Csrf\TokenGenerator\TokenGeneratorInterface;

/**
 *  @Route("/api")
 */
class SecurityController extends AbstractController
{
    private $message;
    public function __construct(MailerService  $mailer)
    {
        $this->mailer = $mailer;
        $this->message = 'message';
    }
    /**
      * @Route("/logincheck", name="login", methods={"POST","GET"})
      */
    public function login(Request $request)
    {
        $user = $this->getUser();

        return $this->json([
            'username' => $user->getUsername(),
            'roles' => $user->getRoles(),
        ]);
    } 

/**
 * @Route("/resetting", name="resetting", methods="GET|POST")
 */
public function resetPassword(Request $request, UserPasswordEncoderInterface $passwordEncoder): Response
{
            $username = $request->query->get('username');
            if ($request->isMethod('GET') && is_null($username)) {
              $this->addFlash('danger', 'Utilisateur non reconnu');
              return $this->redirectToRoute('login');
            }
            if ($request->isMethod('POST')) {
                $username = $request->request->get('username');
                $password = $request->request->get('password');
                $confirm = $request->request->get('confirmepassword');
                if ($password && $password !== $confirm) {
                  $this->addFlash('danger', 'Mot de passe et confirmer mot de passe non identique');
                  return $this->redirectToRoute('login', [], UrlGeneratorInterface::ABSOLUTE_URL);
                }
                $entityManager = $this->getDoctrine()->getManager();
                $user = $entityManager->getRepository(User::class)->findOneBy(['username' => $username]);
                if ($user === null) {
                    $this->addFlash('danger', 'Utilisateur non reconnu');
                    return $this->redirectToRoute('login');
                }
                $user->setPassword($passwordEncoder->encodePassword($user, $password));
                $user->setConfirmepassword($passwordEncoder->encodePassword($user, $confirm));
                //$user->resetPassword();
                $entityManager->flush();
                if (0 === $user->getStatut() && is_null($user->getUsername())) {
                  $subject = 'Administrateur - Mot de passe oublié - Réinisialisation';
                  $partMessage = "Votre mot de passe est maintenant modifié.";
                  $url = $this->generateUrl('login');
                  $message = $this->renderView(
                    '/api/loginchek',
                      [
                          'user'=>$user,
                          'partMessage'=>$partMessage,
                          'url'=>$url
                      ]
                  );
                  $from = ['digitalhakimsa@gmail.com'=> 'Administrateur - Réinisialisation mon mot de passe'];
                  $to = $user->getUsename();
                  $response = $this->mailer->mailer($subject, $from, $to, $message);
                  
                }
                $this->addFlash('notice', 'Mot de passe mis à jour !');
                return $this->redirectToRoute('login');
            }else {
                return $this->render('api/resetting', ['username' => $username]);
            }
        }
}