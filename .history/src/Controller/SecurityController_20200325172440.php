<?php

namespace App\Controller;

use App\Entity\User;
use App\Service\MailerService;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Csrf\TokenGenerator\TokenGeneratorInterface;

/**
 *  @Route("/api")
 */
class SecurityController extends AbstractController
{
    private $message;
    public function __construct(MailerService  $mailer)
    {
        $this->mailer = $mailer;
        $this->message = 'message';
    }
    /**
      * @Route("/logincheck", name="login", methods={"POST","GET"})
      */
    public function login(Request $request)
    {
        $user = $this->getUser();

        return $this->json([
            'username' => $user->getUsername(),
            'roles' => $user->getRoles(),
        ]);
    } 

/**
 * @Route("/resetting", name="resetting", methods="GET|POST")
 */
public function resetPassword(Request $request, UserPasswordEncoderInterface $passwordEncoder): Response
{
            $token = $request->query->get('token');
            if ($request->isMethod('GET') && is_null($token)) {
              $this->addFlash('danger', 'Utilisateur non reconnu');
              return $this->redirectToRoute('logincheck');
            }
            if ($request->isMethod('POST')) {
                $password = $request->request->get('password');
                $confirm = $request->request->get('confirm');
                $token = $request->request->get('token');
                if ($password && $password !== $confirm) {
                  $this->addFlash('danger', 'Mot de passe et confirmer mot de passe non identique');
                  return $this->redirectToRoute('logincheck', [], UrlGeneratorInterface::ABSOLUTE_URL);
                }
                $entityManager = $this->getDoctrine()->getManager();
                $user = $entityManager->getRepository(User::class)->findOneBy(['token' => $token]);
                if ($user === null) {
                    $this->addFlash('danger', 'Utilisateur non reconnu');
                    return $this->redirectToRoute('logincheck');
                }
                $user->setPassword($passwordEncoder->encodePassword($user, $password));
                $user->resetToken();
                $entityManager->flush();
                if (0 === $user->getDisabled() && is_null($user->getToken())) {
                  $subject = 'Afyacare - Mot de passe oublié - Réinisialisation';
                  $partMessage = "Votre mot de passe est maintenant modifié.";
                  $url = $this->generateUrl('logincheck');
                  $message = $this->renderView(
                    'email/templates/confirme.html.twig',
                      [
                          'user'=>$user,
                          'partMessage'=>$partMessage,
                          'url'=>$url
                      ]
                  );
                  $from = ['digitalhakimsa@gmail.com'=> 'Administrateur - Réinisialisation mon mot de passe'];
                  $to = $user->getUsename();
                  $response = $this->mailer->mailer($subject, $from, $to, $message);
                  
                }
                $this->addFlash('notice', 'Mot de passe mis à jour !');
                return $this->redirectToRoute('logincheck');
            }else {
                return $this->render('security/resetting.html.twig', ['token' => $token]);
            }
        }
}