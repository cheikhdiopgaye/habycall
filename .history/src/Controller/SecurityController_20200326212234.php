<?php

namespace App\Controller;

use App\Entity\User;
use App\Entity\Wallet;
use App\Form\UserType;
use App\Service\MailerService;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Csrf\TokenGenerator\TokenGeneratorInterface;

/**
 *  @Route("/api")
 */
class SecurityController extends AbstractController
{
    private $message;
    public function __construct(MailerService  $mailer)
    {
        $this->mailer = $mailer;
        $this->message = 'message';
    }
    /**
      * @Route("/logincheck", name="login", methods={"POST","GET"})
      */
    public function login(Request $request)
    {
        $user = $this->getUser();

        return $this->json([
            'username' => $user->getUsername(),
            'roles' => $user->getRoles(),
        ]);
    } 

/**
 * @Route("/resetting", name="resetting", methods="GET|POST")
 */
public function resetPassword(User $user, Request $request, UserPasswordEncoderInterface $passwordEncoder): Response
{
            $username = $request->query->get('username');
            if ($request->isMethod('GET') && is_null($username)) {
              $this->addFlash('danger', 'Utilisateur non reconnu');
              return $this->redirectToRoute('login');
            }
            if ($request->isMethod('POST')) {
                $username = $user->getUsername();
                $ancienPassword=$user->getPassword();
                $ancienconfirm = $user->getConfirmepassword();
                $form = $this->createForm(UserType::class, $user);
                $data=json_decode($request->getContent(),true);//si json
               if(!$data){
                  $data=$request->request->all();//si non json
                 }
                if ($password && $password !== $confirm) {
                  $this->addFlash('danger', 'Mot de passe et confirmer mot de passe non identique');
                  return $this->redirectToRoute('login', [], UrlGeneratorInterface::ABSOLUTE_URL);
                }
                $entityManager = $this->getDoctrine()->getManager();
                $user = $entityManager->getRepository(User::class)->findOneBy(['username' => $username]);
                if ($user === null) {
                    $this->addFlash('danger', 'Utilisateur non reconnu');
                    return $this->redirectToRoute('login');
                }
                $password=md5(uniqid()).'.'.$file->guessExtension();//on change le nom du fichier
                $user->setPassword($fileName);
                $file->move($this->getParameter($this->imageAng),$fileName); //definir le image_directory dans service.yaml
                $ancienPhoto=$this->getParameter($this->imageAng)."/".$ancienImage;
                if($ancienImage){
                    unlink($ancienPhoto);//supprime l'ancienne
                }
                $user->setPassword($passwordEncoder->encodePassword($user, $password));
                $user->setConfirmepassword($passwordEncoder->encodePassword($user, $confirm));
                //$user->resetPassword();
                $entityManager->flush();
                if (0 === $user->getUsername()) {
                  $subject = 'Administrateur - Mot de passe oublié - Réinisialisation';
                  $partMessage = "Votre mot de passe est maintenant modifié.";
                  $url = $this->generateUrl('login');
                  $message = $this->renderView(
                    '/api/loginchek',
                      [
                          'user'=>$user,
                          'partMessage'=>$partMessage,
                          'url'=>$url
                      ]
                  );
                  $from = ['digitalhakimsa@gmail.com'=> 'Administrateur - Réinisialisation mon mot de passe'];
                  $to = $user->getUsename();
                  $response = $this->mailer->mailer($subject, $from, $to, $message);
                  
                }
                $this->addFlash('notice', 'Mot de passe mis à jour !');
                return $this->redirectToRoute('login');
            }else {
                return $this->render('api/resetting', ['username' => $username]);
            }
        }
}

if(!$candidat){
  throw new HttpException(404,'Cet utilisateur n\'existe pas !');
}

$ancienPassword=$candidat->getPassword();
$ancienConfirmepassword=$candidat->getConfirmepassword();
$form = $this->createForm(UpdatecandidatType::class, $candidat);
$data=json_decode($request->getContent(),true);//si json
if(!$data){
  $data=$request->request->all();//si non json
}
     $ancienImage=$candidat->getPhoto();

$form->submit($data);
if(!$form->isSubmitted()){
  return $this->handleView($this->view($validator->validate($form)));
}

if(!$candidat->getPhoto()){//s il ne change pas sa photo
  $candidat->setPhoto($ancienImage);
}


if($requestFile=$request->files->all()){
  $file=$requestFile['photo'];

  if($file->guessExtension()!='png' && $file->guessExtension()!='jpeg' && $file->guessExtension()!='jpg'){
      throw new HttpException(400,'Entrer une image valide !! ');
  }

  $fileName=md5(uniqid()).'.'.$file->guessExtension();//on change le nom du fichier
  $candidat->setPhoto($fileName);
  $file->move($this->getParameter($this->imageAng),$fileName); //definir le image_directory dans service.yaml
  $ancienPhoto=$this->getParameter($this->imageAng)."/".$ancienImage;
  if($ancienImage){
      unlink($ancienPhoto);//supprime l'ancienne
  }

}

$candidat->setPassword($ancienPassword);
$candidat->setStatut($candidat->getStatut());
$candidat->setConfirmepassword($ancienConfirmepassword);


$manager->persist($candidat); 