<?php

namespace App\Controller;


use App\Entity\Cv;
use App\Entity\User;
use App\Form\CvType;
use App\Entity\Offre;
use App\Entity\Video;
use App\Entity\Langue;
use App\Entity\Metier;
use App\Entity\Secteur;
use App\Form\OffreType;
use App\Form\VideoType;
use App\Entity\Cvlangue;
use App\Entity\Rubrique;
use App\Form\LangueType;
use App\Form\MetierType;
use App\Entity\Formation;
use App\Entity\Useroffre;
use App\Form\SecteurType;
use App\Entity\Experience;
use App\Form\CvlangueType;
use App\Form\RubriqueType;
use App\Form\FormationType;
use App\Form\UseroffreType;
use App\Entity\Infocandidat;
use App\Form\ExperienceType;
use App\Form\InfocandidatType;
use App\Repository\CvRepository;
use App\Repository\UserRepository;
use App\Repository\OffreRepository;
use App\Repository\UseroffreRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/** 
* @Route("/api")
*/
class OffreController extends FOSRestController
{
    private $correcte;
    private $imageAng;

    public function __construct()
    {
        $this-> correcte="Correcte";
        $this-> image_directory="image_directory";
        $this-> imageAng="image_ang";
    }

/**
 * @Route("/security/annonce", name="annonce", methods={"POST"})
 * @IsGranted({"ROLE_ANNONCEUR"}, statusCode=403, message="Vous n'avez pas accès à cette page !" )
 */

public function faireannonce (Request $request, EntityManagerInterface $entityManager, SerializerInterface $serializer,ValidatorInterface $validator)
{
    $offre = new Offre();
    $connecte=$this->getUser();
    $user = $this->getDoctrine()->getRepository(User::class)->find($connecte);
    $offre->setOffruser($user); 
    $form=$this->createForm(OffreType::class , $offre);
    $form->handleRequest($request);
    $data=$request->request->all();
     $form->submit($data);
    // dd($data);
     $entityManager = $this->getDoctrine()->getManager();

     $rubrique =new Rubrique();
     $form=$this->createForm(RubriqueType::class , $rubrique);
     $form->submit($data);

     $entityManager = $this->getDoctrine()->getManager();
     $errors = $validator->validate($rubrique);
     if(count($errors)) {
         $errors = $serializer->serialize($errors, 'json');
         return new Response($errors, 500, [
             'Content-Typle' => 'applicatlion/json'
         ]);
     } 
     $rubrique->addOffre($offre);

     $metier =  new Metier();
     $form=$this->createForm(MetierType::class , $metier);
     $form->submit($data);

     $entityManager = $this->getDoctrine()->getManager();
     $errors = $validator->validate($metier);
     if(count($errors)) {
         $errors = $serializer->serialize($errors, 'json');
         return new Response($errors, 500, [
             'Content-Typle' => 'applicatlion/json'
         ]);
     } 
     $metier->addOffre($offre);

     $secteur = new Secteur();
     $form=$this->createForm(SecteurType::class , $secteur);
     $form->submit($data);

     $entityManager = $this->getDoctrine()->getManager();
     $errors = $validator->validate($secteur);
     if(count($errors)) {
         $errors = $serializer->serialize($errors, 'json');
         return new Response($errors, 500, [
             'Content-Typle' => 'applicatlion/json'
         ]);
     } 
     $metier->setMetiersecteur($secteur);

     $entityManager->persist($rubrique);
     $entityManager->persist($secteur);
     $entityManager->persist($metier); 
     $entityManager->persist($offre);
     $entityManager->flush();
 return new Response('L/annonce a été bien ajouté ',Response::HTTP_CREATED); 

}

/**
 * @Route("/security/parannonceur", name="parannonceur", methods={"GET"})
 * @IsGranted({"ROLE_ANNONCEUR"}, statusCode=403, message="Vous n'avez pas accès à cette page !" )
 */

public function listeposter(OffreRepository $offreRepo, SerializerInterface $serializer,EntityManagerInterface $entityManager)
{
    $connecte = $this->getUser()->getOffruser();
    $offre=$offreRepo->findBy(['user'=>$connecte]);
    $data=$serializer->serialize($offre, 'json', ['groups' => ['offreuser']]);
    return new Response($data, 200, [
        'content_Type' => 'application/json'
    ]);

}

/**
 * @Route("/security/lister/annonces", name="offreannonceur", methods={"GET"})
 * @IsGranted({"ROLE_SUPERADMIN"}, statusCode=403, message="Vous n'avez pas accès à cette page !") */
public function ListerOffre(OffreRepository $offreRepository, SerializerInterface $serializer)
{
    $par = $offreRepository->findAll();
    $users = $serializer->serialize($par, 'json', ['groups' => ['offre']]);
    return new Response($users, 200, [
        'Content-Type' => 'application/json'
    ]);
}

/**
 * @Route("/security/parcandidat", name="listecandidat", methods={"GET"})
 * @IsGranted({"ROLE_ANNONCEUR", "ROLE_CANDIDAT"}, statusCode=403, message="Vous n'avez pas accès à cette page !" )
 */

public function listpostule(UseroffreRepository $useroffreRepo, SerializerInterface $serializer,EntityManagerInterface $entityManager)
{
    $connecte = $this->getUser();
    $offre=$useroffreRepo->findBy(['useroffre'=>$connecte]);
    $data=$serializer->serialize($offre, 'json', ['groups' => ['offreuser']]);
    return new Response($data, 200, [
        'content_Type' => 'application/json'
    ]);

}

/**
* @Route("/security/cv", name="cv", methods={"POST"})
* @IsGranted({"ROLE_CANDIDAT"}, statusCode=403, message="Vous n'avez pas accès à cette page !")
*/

public function ajoutcv (Request $request, EntityManagerInterface $entityManager, SerializerInterface $serializer,ValidatorInterface $validator)
    {
        $cv = new Cv(); 
        $connecte=$this->getUser();
        $user = $this->getDoctrine()->getRepository(User::class)->find($connecte);
         $user->setCvuser($cv);
             // dd($user);
        $form=$this->createForm(CvType::class , $cv);
        $form->handleRequest($request);
        $data=$request->request->all();
         $form->submit($data);
         $cv->setStatut('Correcte');
         $entityManager = $this->getDoctrine()->getManager();
         $info = new Infocandidat();
         $form=$this->createForm(InfocandidatType::class , $info);
         $form->submit($data);
         $entityManager = $this->getDoctrine()->getManager();
         $errors = $validator->validate($info);
         if(count($errors)) {
             $errors = $serializer->serialize($errors, 'json');
             return new Response($errors, 500, [
                 'Content-Typle' => 'applicatlion/json'
             ]);
         }
         $info->addCv($cv);

         $entityManager->persist($info);

         $entityManager->persist($cv);

         $entityManager->flush();
         return new Response('Le Cv a bien été ajouteé',Response::HTTP_CREATED); 
        }



/**
* @Route("/security/experience", name="experience", methods={"POST"})
* @IsGranted({"ROLE_CANDIDAT"}, statusCode=403, message="Vous n'avez pas accès à cette page !")
*/

public function ajoutexperience (Request $request, EntityManagerInterface $entityManager, SerializerInterface $serializer,ValidatorInterface $validator)
    {
         $experience = new Experience();
        $connecte=$this->getUser()->getCvuser();
        $user = $this->getDoctrine()->getRepository(User::class)->find($connecte);
        $cv = $this->getDoctrine()->getRepository(Cv::class)->find($connecte);
        $cv->setExperience($experience);
       // dd($cv);
         $form=$this->createForm(ExperienceType::class , $experience);
         $form->handleRequest($request);
         $data=$request->request->all();
          $form->submit($data);
          $entityManager = $this->getDoctrine()->getManager();
          $errors = $validator->validate($experience);
          if(count($errors)) {
              $errors = $serializer->serialize($errors, 'json');
              return new Response($errors, 500, [
                  'Content-Typle' => 'applicatlion/json'
              ]);
          } 
         $entityManager->persist($experience);
         $entityManager->flush();
        return new Response('Le Candidat a bien ajouté ses expériences',Response::HTTP_CREATED); 
    }



/**
* @Route("/security/formation", name="formation", methods={"POST"})
* @IsGranted({"ROLE_CANDIDAT"}, statusCode=403, message="Vous n'avez pas accès à cette page !")
*/

public function ajoutformation (Request $request, EntityManagerInterface $entityManager, SerializerInterface $serializer,ValidatorInterface $validator)
    {
         $formation = new Formation();
        $connecte=$this->getUser()->getCvuser();
        $user = $this->getDoctrine()->getRepository(User::class)->find($connecte);
        $cv = $this->getDoctrine()->getRepository(Cv::class)->find($connecte);
        $cv->setFormation($formation);


         $form=$this->createForm(FormationType::class , $formation);
         $form->handleRequest($request);
         $data=$request->request->all();
          $form->submit($data);
          $entityManager = $this->getDoctrine()->getManager();
          $errors = $validator->validate($formation);
          if(count($errors)) {
              $errors = $serializer->serialize($errors, 'json');
              return new Response($errors, 500, [
                  'Content-Typle' => 'applicatlion/json'
              ]);
          } 
         $entityManager->persist($formation);
         $entityManager->flush();
        return new Response('Le Candidat a bien ajouté ses formations',Response::HTTP_CREATED); 
    }

/**
* @Route("/security/video", name="video", methods={"POST"})
* @IsGranted({"ROLE_CANDIDAT"}, statusCode=403, message="Vous n'avez pas accès à cette page !")
*/

public function ajoutvideo (Request $request, EntityManagerInterface $entityManager, SerializerInterface $serializer,ValidatorInterface $validator)
    {
        $video = new Video();
        $connecte=$this->getUser()->getCvuser();
        $user = $this->getDoctrine()->getRepository(User::class)->find($connecte);
        $cv = $this->getDoctrine()->getRepository(Cv::class)->find($connecte);
        $cv->setVideo($video);
         $form=$this->createForm(VideoType::class , $video);
         $data = json_decode($request->getContent(),true);
         if(!$data){//s il n'existe pas donc on recupere directement le tableau via la request
             $data=$request->request->all();
         }
         $form->submit($data);
         if($requestFile=$request->files->all()){
 
             $file=$requestFile['image'];
             $extension=$file->guessExtension();
             if($extension!='mp3' && $extension!='mp4' && $extension!='mkv'){
                 throw new HttpException(400,'Entrer une video valide !! ');
             }
 
             $fileName=md5(uniqid()).'.'.$extension;//on change le nom du fichier
             $video->setImage($fileName);
             $file->move($this->getParameter($this->imageAng),$fileName);
         }
         $video->setStatus('Correcte');
         $video->setDatedenregistrement(new \Datetime());
         $entityManager->persist($video);
         $entityManager->flush();
        return new Response('La video du candidat a été bien ajouté',Response::HTTP_CREATED); 
    }

/**
* @Route("/security/langue", name="langue", methods={"POST"})
* @IsGranted({"ROLE_CANDIDAT"}, statusCode=403, message="Vous n'avez pas accès à cette page !")
*/

public function ajoutlangue (Request $request, EntityManagerInterface $entityManager, SerializerInterface $serializer,ValidatorInterface $validator)
    {
        $langue = new Langue();
         $form=$this->createForm(LangueType::class ,$langue);
         $form->submit($data);
         $entityManager = $this->getDoctrine()->getManager();
         $errors = $validator->validate($langue);
         if(count($errors)) {
             $errors = $serializer->serialize($errors, 'json');
             return new Response($errors, 500, [
                 'Content-Typle' => 'applicatlion/json'
             ]);
         }
        $cvlangue = new Cvlangue();

        $connecte = $this->getUser();
        $user = $this->getDoctrine()->getRepository(User::class)->find($connecte);
        //dd($offre);
        $user->setCvuser($cv); 

        $form=$this->createForm(CvlangueType::class , $cvlangue);
        $form->handleRequest($request);
        $data=$request->request->all();
         $form->submit($data);
         //dd($data);
         $entityManager = $this->getDoctrine()->getManager();
         

         $cvlangue->setCv($cv);
         $cvlangue->setLangue($langue);

         //dd($useroffre);
        
         //$offre->addUseroffre($useroffre);
         $entityManager->persist($langue);
         $entityManager->persist($cvlangue);
        $entityManager->flush();
    return new Response('La langue a été bien ajouté ',Response::HTTP_CREATED); 
    }

 /**
 * @Route("/security/listecv", name="listecv", methods={"GET"})
 * @IsGranted({"ROLE_CANDIDAT"}, statusCode=403, message="Vous n'avez pas accès à cette page !" )
 */

public function listecv (CvRepository $cvRepo, SerializerInterface $serializer,EntityManagerInterface $entityManager)
{
    $connecte = $this->getUser()->getCvuser();
    $cv=$cvRepo->find($connecte);
    $data=$serializer->serialize($cv, 'json', ['groups' => ['offreuser']]);
    return new Response($data, 200, [
        'content_Type' => 'application/json'
    ]);

}   
    /**
     * @Route("/security/postuler", name="postuler", methods={"POST"})
     * @IsGranted({"ROLE_CANDIDAT"}, statusCode=403, message="Vous n'avez pas accès à cette page !")
     */
    public function postuler(Request $request, EntityManagerInterface $entityManager, SerializerInterface $serializer,ValidatorInterface $validator)
        {
            $donnes = json_decode($request->getContent());
        
            $useroffre = new Useroffre();
        
            $user = $this->getUser();
        
            $offre = $this->getDoctrine()->getRepository(Offre::class)->find($donnes->offre_id);
        
            $useroffre->setUseroffre($user);
            $useroffre->setUserof($offre);
        
            $entityManager->persist($useroffre);
            $entityManager->flush();
            return new Response('L/annonce a été bien ajouté ', Response::HTTP_CREATED);
          }
    
}
