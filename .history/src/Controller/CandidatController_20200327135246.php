<?php

namespace App\Controller;

use App\Entity\User;
use App\Entity\Offre;
use App\Form\CandidatType;
use App\Service\MailerService;
use App\Form\UpdatecandidatType;
use App\Repository\UserRepository;
use App\Repository\MetierRepository;
use App\Repository\SecteurRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 *  @Route("/api")
 */
class CandidatController extends FOSRestController
{
    private $connecter;
    private $deconnecter;
    private $imageAng;
    private $message;

    public function __construct(MailerService  $mailer)
    {
        $this-> connecter="Connecter";
        $this-> deconnecter="Deconnecter";
        $this-> image_directory="image_directory";
        $this-> imageAng="image_ang";
        $this->mailer = $mailer;
        $this->message = 'message';

    }
   

    /**
     * @Route("/inscriptionC", name="inscriptionC", methods={"POST"})
     */
    public function inscriptionCandidat(Request $request,EntityManagerInterface $manager, UserPasswordEncoderInterface $passwordEncoder, ValidatorInterface $validator,  \Swift_Mailer $mailer)
    {

        $candidat=new User();
        $form = $this->createForm(CandidatType::class, $candidat);
        $data = json_decode($request->getContent(),true);
        if(!$data){//s il n'existe pas donc on recupere directement le tableau via la request
            $data=$request->request->all();
        }
        $form->submit($data);
        if(!$form->isSubmitted() || !$form->isValid()) {
            return $this->handleView($this->view($validator->validate($form)));
        
        }
        $roles=["ROLE_CANDIDAT"];
        $candidat->setRoles($roles);

        if($requestFile=$request->files->all()){

            $file=$requestFile['photo'];
            $extension=$file->guessExtension();
            if($extension!='png' && $extension!='jpeg' && $extension!='jpg'){
                throw new HttpException(400,'Entrer une image valide !! ');
            }

            $fileName=md5(uniqid()).'.'.$extension;//on change le nom du fichier
            $candidat->setPhoto($fileName);
            $file->move($this->getParameter($this->imageAng),$fileName);
        }

        $candidat->setStatut('Connecter');
        $candidat->setPassword($passwordEncoder->encodePassword($candidat,
        $form->get('password')->getData()
            )
            );
        $candidat->setConfirmePassword($passwordEncoder->encodePassword($candidat,
        $form->get('confirmepassword')->getData()
            )
            );
        $manager->persist($candidat);
        $message= 'Inscription réussie';
        $response = $this->mailer->mailer('Bienvenue dans l\'application Ujob','',$candidat->getUsername(),$message);
        $manager->flush();
        return $this->handleView($this->view("Le candidat s'est bien inscrit",Response::HTTP_CREATED));
    }

 
    /**
    * @Route("/candidat/update/{id}", name="update_candidat", methods={"POST"})
    */
    public function updateCandidat(User $candidat,Request $request, EntityManagerInterface $manager, ValidatorInterface $validator,UserPasswordEncoderInterface $encoder){

        if(!$candidat){
            throw new HttpException(404,'Cet utilisateur n\'existe pas !');
        }

        $ancienPassword=$candidat->getPassword();
        $ancienConfirmepassword=$candidat->getConfirmepassword();
        $form = $this->createForm(UpdatecandidatType::class, $candidat);
        $data=json_decode($request->getContent(),true);//si json
        if(!$data){
            $data=$request->request->all();//si non json
        }
               $ancienImage=$candidat->getPhoto();

        $form->submit($data);
        if(!$form->isSubmitted()){
            return $this->handleView($this->view($validator->validate($form)));
        }

        if(!$candidat->getPhoto()){//s il ne change pas sa photo
            $candidat->setPhoto($ancienImage);
        }


        if($requestFile=$request->files->all()){
            $file=$requestFile['photo'];

            if($file->guessExtension()!='png' && $file->guessExtension()!='jpeg' && $file->guessExtension()!='jpg'){
                throw new HttpException(400,'Entrer une image valide !! ');
            }

            $fileName=md5(uniqid()).'.'.$file->guessExtension();//on change le nom du fichier
            $candidat->setPhoto($fileName);
            $file->move($this->getParameter($this->imageAng),$fileName); //definir le image_directory dans service.yaml
            $ancienPhoto=$this->getParameter($this->imageAng)."/".$ancienImage;
            if($ancienImage){
                unlink($ancienPhoto);//supprime l'ancienne
            }

        }

        $candidat->setPassword($ancienPassword);
        $candidat->setStatut($candidat->getStatut());
        $candidat->setConfirmepassword($ancienConfirmepassword);


        $manager->persist($candidat); 
        $manager->flush();
     
        return $this->handleView($this->view("La modification s'est bien passer",Response::HTTP_OK));
            
    }
    
    /**
    * @Route("/security/candidat/bloquer/{id}", name="bloquer_debloquer_candidat", methods={"GET"})
    */ 
    public function bloquerCandidat(EntityManagerInterface $manager,User $candidat=null)
    {
        if($candidat->getStatut() == $this->connecter){
            $candidat->setStatut($this->deconnecter);
            $texte= 'Candidat déconnecter';
        }
        else  if($candidat->getStatut() == $this->deconnecter)
        {
            $candidat->setStatut($this->connecter);
            $texte='Candidat connecter';
        }
        $manager->persist($candidat);
        $manager->flush();
        return $this->handleView($this->view("Candidat déconnecté",Response::HTTP_OK));
    }

    /**
    * @Route("/security/lister/candidats", name="candidat_user", methods={"GET"})
    * @IsGranted({"ROLE_SUPERADMIN"}, statusCode=403, message="Vous n'avez pas accès à cette page !")
    */
public function show( UserRepository $userRepository, SerializerInterface $serializer)
    {
        $profil= $userRepository->findAll();
        $tab =[];
        for($i=0; $i<count($profil);$i++){
            if($profil[$i]->getRoles()[0]=="ROLE_CANDIDAT") {
                array_push($tab,$profil[$i]);
        }
        }  
        return $this->json($tab,200,[],['groups' => ['show']]);
    
    }
    
/* *
 * @Route("/security/lister/{metier}", name="lister_metier", methods={"GET"})
 * @IsGranted({"ROLE_CANDIDAT"}, statusCode=403, message="Vous n'avez pas accès à cette page !" )
 */

/* public function ListerMetier (Offre $metier, SerializerInterface $serializer)
{
    $user= $this->getDoctrine()->getRepository(Offre::class)->find($metier->getMetier());
    $data = $serializer->serialize($user, 'json');
    return new Response($data, 200, [
        'Content-Type' => 'application/json'
    ]);
} */

/**
 * @Route("/security/lister/metier", name="metier", methods={"GET"})
 */
public function ListerMetier(MetierRepository $metierRepository, SerializerInterface $serializer)
{
    $par = $metierRepository->findAll();
    $users = $serializer->serialize($par, 'json', ['groups' => ['offreuser']]);
    return new Response($users, 200, [
        'Content-Type' => 'application/json'
    ]);
}

/**
 * @Route("/security/lister/metier", name="metier", methods={"GET"})
 */
public function ListerSecteur(SecteurRepository $secteurRepository, SerializerInterface $serializer)
{
    $par = $secteurRepository->findAll();
    $users = $serializer->serialize($par, 'json', ['groups' => ['offreuser']]);
    return new Response($users, 200, [
        'Content-Type' => 'application/json'
    ]);
}

}