<?php

namespace App\Controller;

use App\Entity\Cv;
use App\Entity\User;
use App\Form\CvType;
use App\Entity\Offre;
use App\Entity\Video;
use App\Entity\Langue;
use App\Entity\Metier;
use App\Entity\Secteur;
use App\Form\OffreType;
use App\Form\VideoType;
use App\Entity\Postuler;
use App\Entity\Rubrique;
use App\Form\LangueType;
use App\Form\MetierType;
use App\Entity\Formation;
use App\Form\SecteurType;
use App\Entity\Experience;
use App\Form\PostulerType;
use App\Form\RubriqueType;
use App\Form\FormationType;
use App\Form\ExperienceType;
use App\Repository\UserRepository;
use App\Repository\OffreRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/** 
* @Route("/api")
*/
class OffreController extends FOSRestController
{
    private $correcte;

    public function __construct()
    {
        $this-> correcte="Correcte";
    }

    /**
     * @Route("/security/offre", name="offre", methods={"POST"})
     * @IsGranted({"ROLE_ANNONCEUR"}, statusCode=403, message="Vous n'avez pas accès à cette page !")
     */
    public function faireannonce(Request $request, EntityManagerInterface $entityManager, SerializerInterface $serializer,ValidatorInterface $validator)
    {
       $offre = new Offre();
       $connecte=$this->getUser();
       $user = $this->getDoctrine()->getRepository(User::class)->find($connecte);
       $offre->setUser($user);
      
       $form=$this->createForm(OffreType::class , $offre);
       $form->handleRequest($request);
       $data=$request->request->all();
        $form->submit($data);
        $offre->setDatedebut(new \Datetime('now'));
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($offre);

        $rubrique =new Rubrique();
        $form=$this->createForm(RubriqueType::class , $rubrique);
        $form->submit($data);
   
        $entityManager = $this->getDoctrine()->getManager();
        $errors = $validator->validate($rubrique);
        if(count($errors)) {
            $errors = $serializer->serialize($errors, 'json');
            return new Response($errors, 500, [
                'Content-Typle' => 'applicatlion/json'
            ]);
        } 
        $rubrique->addOffre($offre);
        $entityManager->persist($rubrique);
        $metier =new Metier();
        $form=$this->createForm(MetierType::class , $metier);
        $form->submit($data);
   
        $entityManager = $this->getDoctrine()->getManager();
        $errors = $validator->validate($metier);
        if(count($errors)) {
            $errors = $serializer->serialize($errors, 'json');
            return new Response($errors, 500, [
                'Content-Typle' => 'applicatlion/json'
            ]);
        } 
        $metier->addOffr($offre);
        $secteur = new Secteur();
        $form=$this->createForm(SecteurType::class , $secteur);
        $form->submit($data);
   
        $entityManager = $this->getDoctrine()->getManager();
        $errors = $validator->validate($secteur);
        if(count($errors)) {
            $errors = $serializer->serialize($errors, 'json');
            return new Response($errors, 500, [
                'Content-Typle' => 'applicatlion/json'
            ]);
        } 
        $metier->setSecteur($secteur);
        
        $entityManager->persist($secteur);
        $entityManager->persist($metier);
        $entityManager->flush();
    return new Response('L/annonce a été bien ajouté ',Response::HTTP_CREATED); 
    }

/**
 * @Route("/security/lister/annonces", name="offre_annonceur", methods={"GET"})
 * @IsGranted({"ROLE_SUPERADMIN"}, statusCode=403, message="Vous n'avez pas accès à cette page !") */
public function ListerOffre(OffreRepository $offreRepository, SerializerInterface $serializer)
{
    $par = $offreRepository->findAll();
    $users = $serializer->serialize($par, 'json', ['groups' => ['offre']]);
    return new Response($users, 200, [
        'Content-Type' => 'application/json'
    ]);
}

/**
 * @Route("/security/lister/parannonceur/{id}", name="lister_annonceur", methods={"GET"})
 * @IsGranted({"ROLE_ANNONCEUR"}, statusCode=403, message="Vous n'avez pas accès à cette page !" )
 */


public function listeuser(UserRepository $userRepo, SerializerInterface $serializer,EntityManagerInterface $entityManager)
{
  
  $listUser= $entityManager->getRepository(Offre::class);
  $users = $listUser->findAll();
 
  $jsonObject = $serializer->serialize($users, 'json', [
      'circular_reference_handler' => function ($object) {
          return $object->getId();
      }
  ]);
  return new Response($jsonObject, 200, ['Content-Type' => 'application/json']);


/*   {
    
    $connecte = $this->getUser();
    $annonceur=$userRepo->findBy(['user'=>$connecte]);
    $data = $serializer->serialize($annonceur, 'json');
    return new Response($data, 200, [
        'Content-Type' => 'application/json'
    ]);
} */
}





     /**
     * @Route("/security/candidater", name="postule", methods={"POST"})
     * @IsGranted({"ROLE_CANDIDAT"}, statusCode=403, message="Vous n'avez pas accès à cette page !")
     */

    public function postuler (Request $request, EntityManagerInterface $entityManager, SerializerInterface $serializer,ValidatorInterface $validator)
    {
        $cv = new Cv();
        $connecte=$this->getUser();
        $user = $this->getDoctrine()->getRepository(User::class)->find($connecte);
        $cv->setCvuser($user);

        $form=$this->createForm(CvType::class , $cv);
        $form->handleRequest($request);
        $data=$request->request->all();
         $form->submit($data);
         $cv->setDatecreation(new \Datetime('now'));
         $cv->setStatut($this->correcte);
         $entityManager = $this->getDoctrine()->getManager();

         $langue = new Langue();
         $form=$this->createForm(LangueType::class , $langue);
         $form->handleRequest($request);
         $data=$request->request->all();
          $form->submit($data);
          $entityManager = $this->getDoctrine()->getManager();
          $errors = $validator->validate($langue);
          if(count($errors)) {
              $errors = $serializer->serialize($errors, 'json');
              return new Response($errors, 500, [
                  'Content-Typle' => 'applicatlion/json'
              ]);
          } 
          $langue->setLangue($cv);

         $formation = new Formation();
         $form=$this->createForm(FormationType::class , $formation);
         $form->handleRequest($request);
         $data=$request->request->all();
          $form->submit($data);
          $entityManager = $this->getDoctrine()->getManager();
          $errors = $validator->validate($formation);
          if(count($errors)) {
              $errors = $serializer->serialize($errors, 'json');
              return new Response($errors, 500, [
                  'Content-Typle' => 'applicatlion/json'
              ]);
          } 
          $formation->setFormer($cv);         

         $experience = new Experience();
         $form=$this->createForm(ExperienceType::class , $experience);
         $form->handleRequest($request);
         $data=$request->request->all();
          $form->submit($data);
          $entityManager = $this->getDoctrine()->getManager();
          $errors = $validator->validate($experience);
          if(count($errors)) {
              $errors = $serializer->serialize($errors, 'json');
              return new Response($errors, 500, [
                  'Content-Typle' => 'applicatlion/json'
              ]);
          } 
          $experience->setExperimenter($cv);         

         $video = new Video();
         $form=$this->createForm(VideoType::class , $video);
         $form->submit($data);
         if($requestFile=$request->files->all()){

            $file=$requestFile['photo'];
            $extension=$file->guessExtension();
            if($extension!='png' && $extension!='jpeg' && $extension!='jpg'){
                throw new HttpException(400,'Entrer une image valide !! ');
            }

            $fileName=md5(uniqid()).'.'.$extension;//on change le nom du fichier
            $video->setImage($fileName);
            $file->move($this->getParameter($this->imageAng),$fileName);
        }
         $video->setStatus($this->correcte);
         $entityManager = $this->getDoctrine()->getManager();
         $errors = $validator->validate($video);
         if(count($errors)) {
             $errors = $serializer->serialize($errors, 'json');
             return new Response($errors, 500, [
                 'Content-Typle' => 'applicatlion/json'
             ]);
         } 
         $video->setFaire($cv);

         $metier = new Metier();
         $form=$this->createForm(MetierType::class , $metier);
         $form->submit($data);
    
         $entityManager = $this->getDoctrine()->getManager();
         $errors = $validator->validate($metier);
         if(count($errors)) {
             $errors = $serializer->serialize($errors, 'json');
             return new Response($errors, 500, [
                 'Content-Typle' => 'applicatlion/json'
             ]);
         } 
         $metier->addCv($cv);

         $postuler = new Postuler();
         $form=$this->createForm(PostulerType::class , $postuler);
         $form->submit($data);
         $postuler->setDatepostule(new \Datetime('now'));
         $postuler->setResultat($this->correcte);
         $entityManager = $this->getDoctrine()->getManager();
         $errors = $validator->validate($metier);
         if(count($errors)) {
             $errors = $serializer->serialize($errors, 'json');
             return new Response($errors, 500, [
                 'Content-Typle' => 'applicatlion/json'
             ]);
         } 
         $postuler->setPoste($cv);
        /*  $connecte=$this->getPostuler()->getPoster();
         $offre = $this->getDoctrine()->getRepository(Offre::class)->find($connecte);
         $postuler->setPoster($offre); */

         $entityManager->persist($langue);
         $entityManager->persist($formation);
         $entityManager->persist($experience);
         $entityManager->persist($video);
         $entityManager->persist($metier);
         $entityManager->persist($postuler);
         $entityManager->persist($cv);
         $entityManager->flush();
        return new Response('Le Candidat a bien postulé',Response::HTTP_CREATED); 
    }
}
