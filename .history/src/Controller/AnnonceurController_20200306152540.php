<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\UserType;
use App\Entity\Entreprise;
use App\Form\EntrepriseType;
use App\Form\UpdateUserType;
use App\Form\UpdateEntrepriseType;
use App\Repository\EntrepriseRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;


/** 
*  @Route("/api")
*/
class AnnonceurController extends FOSRestController {

    private $connecter;
    private $deconnecter;
    private $imageAng;

    public function __construct()
    {
        $this -> connecter ="Connecter";
        $this -> deconnecter ="Deconnecter";
        $this-> image_directory="image_directory";
        $this-> imageAng="image_ang";
    }

/**
* @Route("/inscriptionannonceur", name="inscriptionannonceur", methods={"POST"})
*/
public function addannonceur(Request $request, EntityManagerInterface $entityManager, UserPasswordEncoderInterface $passwordEncoder, SerializerInterface $serializer,ValidatorInterface $validator, \Swift_Mailer $mailer): Response
{


    $entreprise= new Entreprise();
    $form = $this->createForm(EntrepriseType::class, $entreprise);
    $data=$request->request->all();
    $form->submit($data);
    if($requestFile=$request->files->all()){

        $file=$requestFile['logo'];
        $extension=$file->guessExtension();
        if($extension!='png' && $extension!='jpeg' && $extension!='jpg'){
            throw new HttpException(400,'Entrer une image valide !! ');
        }
        $fileName=md5(uniqid()).'.'.$extension;//on change le nom du fichier
        $entreprise->setLogo($fileName);
        $file->move($this->getParameter($this->imageAng),$fileName);
    }
    $entityManager = $this->getDoctrine()->getManager();
    $errors = $validator->validate($entreprise);
    if(count($errors)) {
        $errors = $serializer->serialize($errors, 'json');
        return new Response($errors, 500, [
            'Content-Typle' => 'applicatlion/json'
        ]);
    } 
    $entityManager->persist($entreprise);

    $utilisateur = new User();
    $form=$this->createForm(UserType::class , $utilisateur);
    $form->submit($data);
    if(!$form->isSubmitted() || !$form->isValid()){
        //return $this->handleView($this->view($validator->validate($form)));
               $message = (new \Swift_Message('Annonceur'))
        // On attribue l'expéditeur
        ->setFrom($utilisateur->'username')
        // On attribue le destinataire
        ->setTo('contact@hakimdigitalsa.com')
        // On crée le message avec la vue Twig
        ->setBody(
            $this->renderView(
                // templates/emails/registration.html.twig
                'templates/emails/registration.html.twig', compact('annonceur')
            ),
            'text/html'
        );
    // On envoi le message
    $mailer->send($message);
    $this->addFlash('message', 'Le message a bien été envoyé');
    
    }
    $utilisateur->setRoles(["ROLE_ANNONCEUR"]);
    $utilisateur->setStatut($this->connecter);
    $utilisateur->setPassword($passwordEncoder->encodePassword($utilisateur,
    $form->get('password')->getData()
        )
        );
    $utilisateur->setConfirmePassword($passwordEncoder->encodePassword($utilisateur,
    $form->get('confirmepassword')->getData()
        )
        );   
    $entityManager = $this->getDoctrine()->getManager();
    $errors = $validator->validate($utilisateur);
          if(count($errors)) {
              $errors = $serializer->serialize($errors, 'json');
              return new Response($errors, 500, [
                  'Content-Typle' => 'applicatlion/json'
              ]);
          } 
    $utilisateur->setEntreprise($entreprise);
    $entityManager->persist($utilisateur);
    $entityManager->flush();
    return $this->render('annonceur/index.html.twig', [
        'userForm' => $form->createView()
    ]);
   // return new Response('Un Annonceur est bien inscrit',Response::HTTP_CREATED); 
}



/** 
* @Route("/security/annonceur/bloquer/{id}", name="bloquer_debloquer_annonceur", methods={"GET"})
*/ 
    public function bloqueAnnonceur(EntityManagerInterface $manager,User $utilisateur=null)
    {
        if($utilisateur->getStatut() == $this->connecter){
            $utilisateur->setStatut($this->deconnecter);
            $texte= 'Annonceur déconnecter';
        }
        else{
            $utilisateur->setStatut($this->connecter);
            $texte='Annonceur connecter';
        }
        $manager->persist($utilisateur);
        $manager->flush();
        $afficher = [ $this->status => 200, $this->message => $texte];
        return $this->handleView($this->view($afficher,Response::HTTP_OK));
    }

/** 
 * @Route("/annonceur/update/{id}", name="update_annonceur", methods={"POST"})
 */
    public function updateAnnonceur(User $annonceur ,Request $request, EntityManagerInterface $manager, ValidatorInterface $validator,UserPasswordEncoderInterface $encoder){
            
        if(!$annonceur){
            throw new HttpException(404,'Cet utilisateur n\'existe pas !');
        }
        $ancienPassword=$annonceur->getPassword();
        $form = $this->createForm(UpdateUserType::class,$annonceur);
        $data=json_decode($request->getContent(),true);//si json
        if(!$data){
            $data=$request->request->all();//si non json
        }                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                
        $form->submit($data);
        if(!$form->isSubmitted()){
            return $this->handleView($this->view($validator->validate($form)));
        }

        $annonceur->setPassword($ancienPassword);
        $annonceur->setStatut($annonceur->getStatut());
        $manager->persist($annonceur); 
        $manager->flush();
        return $this->handleView($this->view("Les informations ont été bien modifié",Response::HTTP_OK));
    }

/**
 * @Route("/entreprise/update/{id}", name="update_entreprise", methods={"POST"})
 */
    public function updateEntreprise( Entreprise $entreprise ,Request $request, EntityManagerInterface $manager, ValidatorInterface $validator,UserPasswordEncoderInterface $encoder){
            
        $form = $this->createForm(UpdateEntrepriseType::class,$entreprise);
        $data=json_decode($request->getContent(),true);//si json
        if(!$data){
            $data=$request->request->all();//si non json
        }
        $ancienneNomLogo=$entreprise->getLogo();
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   
        $form->submit($data);
        if(!$form->isSubmitted()){
            return $this->handleView($this->view($validator->validate($form)));
        }
        if(!$entreprise->getLogo()){//s il ne change pas sa photo
            $entreprise->setLogo($ancienneNomLogo);
        }
        if($requestFile=$request->files->all()){
            $file=$requestFile['logo'];

            if($file->guessExtension()!='png' && $file->guessExtension()!='jpeg' && $file->guessExtension()!='jpg'){
                throw new HttpException(400,'Entrer une image valide !! ');
            }

            $fileName=md5(uniqid()).'.'.$file->guessExtension();//on change le nom du fichier
            $entreprise->setLogo($fileName);
            $file->move($this->getParameter($this->imageAng),$fileName); //definir le image_directory dans service.yaml
            $ancienPhoto=$this->getParameter($this->imageAng)."/".$ancienneNomLogo;
            if($ancienneNomLogo){
                unlink($ancienPhoto);//supprime l'ancienne
            }

        }
     
        $manager->persist($entreprise); 
        $manager->flush();
        return $this->handleView($this->view("Les informations ont été bien modifié",Response::HTTP_OK));
    }

/**
 * @Route("/security/lister/users", name="user_entreprise", methods={"GET"})
 * @IsGranted({"ROLE_SUPERADMIN"}, statusCode=403, message="Vous n'avez pas accès à cette page !")
 */
public function ListerAnnonceur(EntrepriseRepository $entrepriseRepository, SerializerInterface $serializer)
{
    $par = $entrepriseRepository->findAll();
    $users = $serializer->serialize($par, 'json', ['groups' => ['entreprise']]);
    return new Response($users, 200, [
        'Content-Type' => 'application/json'
    ]);
}

}